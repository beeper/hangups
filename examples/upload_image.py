"""Example of using hangups to upload an image."""

import random

import hangups

from common import run_example


async def upload_image(client, args):
    group_id = None
    if args.type == 'dm':
        group_id = hangups.googlechat_pb2.GroupId(
            dm_id=hangups.googlechat_pb2.DmId(
                dm_id=args.conversation_id,
            )
        )
    elif args.type == 'space':
        group_id = hangups.googlechat_pb2.GroupId(
            space_id=hangups.googlechat_pb2.SpaceId(
                space_id=args.conversation_id,
            )
        )
    else:
        raise Exception('Unknown conversation type {}'.format(args.type))

    # Upload image to obtain the UploadMetadata:
    image_file = open(args.image, 'rb')
    upload_metadata = await client.upload_image(
        image_file, args.conversation_id
    )

    # Send a chat message referencing the uploaded image_id:
    request = hangups.googlechat_pb2.CreateTopicRequest(
        request_header=client.get_gc_request_header(),
        group_id=group_id,
        local_id=f'hangups%{random.randint(0, 0xffffffffffffffff)}',
        history_v2=True,
        text_body="",
        annotations=[
            hangups.googlechat_pb2.Annotation(
                type=hangups.googlechat_pb2.AnnotationType.UPLOAD_METADATA,
                upload_metadata=upload_metadata,
                chip_render_type=hangups.googlechat_pb2.Annotation.ChipRenderType.RENDER,
            )
        ],
    )
    await client.create_topic(request)

if __name__ == '__main__':
    run_example(upload_image, '--conversation-id', '--image', '--type')
