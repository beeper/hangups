.. This file was automatically generated from hangups/googlechat.proto and should not be edited directly.

UserId
------

============ ====== =========== ======== =========================================
Field        Number Type        Label    Description                              
============ ====== =========== ======== =========================================
:code:`id`   1      string      optional                                          
:code:`type` 2      `UserType`_ optional optional DYNProtoAppId origin_app_id = 3;
============ ====== =========== ======== =========================================

User
----

========================== ====== ======================== ======== =========================================================================================================================================================================================
Field                      Number Type                     Label    Description                                                                                                                                                                              
========================== ====== ======================== ======== =========================================================================================================================================================================================
:code:`user_id`            1      `UserId`_                optional                                                                                                                                                                                          
:code:`name`               2      string                   optional                                                                                                                                                                                          
:code:`avatar_url`         3      string                   optional                                                                                                                                                                                          
:code:`email`              4      string                   optional                                                                                                                                                                                          
:code:`first_name`         5      string                   optional                                                                                                                                                                                          
:code:`last_name`          6      string                   optional                                                                                                                                                                                          
:code:`deleted`            7      bool                     optional                                                                                                                                                                                          
:code:`is_anonymous`       8      bool                     optional                                                                                                                                                                                          
:code:`gender`             10     string                   optional optional DYNProtoBotInfo bot_info = 9;                                                                                                                                                   
:code:`block_relationship` 13     `UserBlockRelationship`_ optional optional DYNProtoUserAccountState user_account_state = 11; optional DYNProtoUserProfileVisibility user_profile_visibility = 15; optional DYNProtoOrganizationInfo organization_info = 12;
========================== ====== ======================== ======== =========================================================================================================================================================================================

UserBlockRelationship
---------------------

=============================== ====== ==== ======== ===========
Field                           Number Type Label    Description
=============================== ====== ==== ======== ===========
:code:`is_blocked_by_requester` 1      bool optional            
:code:`has_blocked_requester`   2      bool optional            
=============================== ====== ==== ======== ===========

DndSettings
-----------

===================================== ====== ============================= ======== ===========
Field                                 Number Type                          Label    Description
===================================== ====== ============================= ======== ===========
:code:`dnd_state`                     1      `DndSettings.DndState_State`_ optional            
:code:`dnd_expiry_time_usec`          2      int64                         optional            
:code:`state_remaining_duration_usec` 3      int64                         optional            
===================================== ====== ============================= ======== ===========

DndSettings.DndState_State
--------------------------

================= ====== ===========
Name              Number Description
================= ====== ===========
:code:`UNKNOWN`   0                 
:code:`AVAILABLE` 1                 
:code:`DND`       2                 
================= ====== ===========

Emoji
-----

=============== ====== ====== ======== ===============
Field           Number Type   Label    Description    
=============== ====== ====== ======== ===============
:code:`unicode` 1      string optional oneof Content {
=============== ====== ====== ======== ===============

CustomStatus
------------

=================================== ====== ======== ======== ===========
Field                               Number Type     Label    Description
=================================== ====== ======== ======== ===========
:code:`status_text`                 1      string   optional            
:code:`status_emoji`                2      string   optional            
:code:`state_expiry_timestamp_usec` 3      int64    optional            
:code:`emoji`                       4      `Emoji`_ optional            
=================================== ====== ======== ======== ===========

UserStatus
----------

=========================================== ====== =============== ======== ===========
Field                                       Number Type            Label    Description
=========================================== ====== =============== ======== ===========
:code:`user_id`                             1      `UserId`_       optional            
:code:`dnd_settings`                        2      `DndSettings`_  optional            
:code:`update_timestamp_usec`               3      int64           optional            
:code:`dnd_settings_update_timestamp_usec`  7      int64           optional            
:code:`status_timestamp_usec`               4      int64           optional            
:code:`presence_shared`                     5      bool            optional            
:code:`custom_status`                       6      `CustomStatus`_ optional            
:code:`custom_status_update_timestamp_usec` 8      int64           optional            
=========================================== ====== =============== ======== ===========

UserStatusUpdatedEvent
----------------------

=================== ====== ============= ======== ===========
Field               Number Type          Label    Description
=================== ====== ============= ======== ===========
:code:`user_status` 1      `UserStatus`_ optional            
=================== ====== ============= ======== ===========

GetUserStatusRequest
--------------------

====================== ====== ================ ======== ===========
Field                  Number Type             Label    Description
====================== ====== ================ ======== ===========
:code:`user_ids`       1      `UserId`_        repeated            
:code:`request_header` 100    `RequestHeader`_ optional            
====================== ====== ================ ======== ===========

GetUserStatusResponse
---------------------

===================== ====== ============= ======== ===========
Field                 Number Type          Label    Description
===================== ====== ============= ======== ===========
:code:`user_statuses` 1      `UserStatus`_ repeated            
===================== ====== ============= ======== ===========

GetSelfUserStatusRequest
------------------------

====================== ====== ================ ======== ===========
Field                  Number Type             Label    Description
====================== ====== ================ ======== ===========
:code:`request_header` 100    `RequestHeader`_ optional            
====================== ====== ================ ======== ===========

GetSelfUserStatusResponse
-------------------------

===================== ====== =============== ======== ===========
Field                 Number Type            Label    Description
===================== ====== =============== ======== ===========
:code:`user_status`   1      `UserStatus`_   optional            
:code:`user_revision` 2      `ReadRevision`_ optional            
===================== ====== =============== ======== ===========

ClientFeatureCapabilities
-------------------------

================================ ====== ============================================ ======== ===========
Field                            Number Type                                         Label    Description
================================ ====== ============================================ ======== ===========
:code:`enable_all_features`      1      bool                                         optional            
:code:`spaces_level_for_testing` 2      `ClientFeatureCapabilities.CapabilityLevel`_ optional            
:code:`dms_level_for_testing`    3      `ClientFeatureCapabilities.CapabilityLevel`_ optional            
:code:`post_rooms_level`         4      `ClientFeatureCapabilities.CapabilityLevel`_ optional            
:code:`spam_room_invites_level`  5      `ClientFeatureCapabilities.CapabilityLevel`_ optional            
:code:`tombstone_level`          6      `ClientFeatureCapabilities.CapabilityLevel`_ optional            
:code:`rich_text_viewing_level`  7      `ClientFeatureCapabilities.CapabilityLevel`_ optional            
================================ ====== ============================================ ======== ===========

ClientFeatureCapabilities.CapabilityLevel
-----------------------------------------

======================= ====== ===========
Name                    Number Description
======================= ====== ===========
:code:`UNSUPPORTED`     0                 
:code:`DATA_SUPPORTED`  1                 
:code:`FULLY_SUPPORTED` 2                 
======================= ====== ===========

RequestHeader
-------------

=================================== ====== ============================ ======== ===========
Field                               Number Type                         Label    Description
=================================== ====== ============================ ======== ===========
:code:`trace_id`                    1      int64                        optional            
:code:`client_type`                 2      `RequestHeader.ClientType`_  optional            
:code:`client_version`              3      int64                        optional            
:code:`locale`                      4      string                       optional            
:code:`client_feature_capabilities` 5      `ClientFeatureCapabilities`_ optional            
=================================== ====== ============================ ======== ===========

RequestHeader.ClientType
------------------------

======================== ====== ===========
Name                     Number Description
======================== ====== ===========
:code:`UNKNOWN`          0                 
:code:`ANDROID`          1                 
:code:`IOS`              2                 
:code:`WEB`              3                 
:code:`CLASSIC_INTEROP`  4                 
:code:`INTERNAL_TESTING` 5                 
:code:`WEB_DESKTOP`      6                 
:code:`WEB_GMAIL`        7                 
======================== ====== ===========

Member
------

============ ====== ======= ======== ===============
Field        Number Type    Label    Description    
============ ====== ======= ======== ===============
:code:`user` 1      `User`_ optional oneof Profile {
============ ====== ======= ======== ===============

MemberId
--------

=============== ====== ========= ======== ===========
Field           Number Type      Label    Description
=============== ====== ========= ======== ===========
:code:`user_id` 1      `UserId`_ optional oneof Id { 
=============== ====== ========= ======== ===========

MembershipId
------------

================= ====== =========== ======== ===========
Field             Number Type        Label    Description
================= ====== =========== ======== ===========
:code:`member_id` 1      `MemberId`_ optional            
:code:`space_id`  2      `SpaceId`_  optional            
:code:`group_id`  3      `GroupId`_  optional            
================= ====== =========== ======== ===========

Membership
----------

======================== ====== ================== ======== ===========
Field                    Number Type               Label    Description
======================== ====== ================== ======== ===========
:code:`id`               1      `MembershipId`_    optional            
:code:`create_time`      2      int64              optional            
:code:`membership_state` 3      `MembershipState`_ optional            
:code:`invite_category`  4      `InviteCategory`_  optional            
:code:`membership_role`  5      `MembershipRole`_  optional            
======================== ====== ================== ======== ===========

MemberProfile
-------------

============== ====== =============== ======== ===========
Field          Number Type            Label    Description
============== ====== =============== ======== ===========
:code:`id`     1      `MembershipId`_ optional            
:code:`member` 2      `Member`_       optional            
============== ====== =============== ======== ===========

GetMembersRequest
-----------------

====================== ====== ================ ======== ===========
Field                  Number Type             Label    Description
====================== ====== ================ ======== ===========
:code:`request_header` 100    `RequestHeader`_ optional            
:code:`member_ids`     1      `MemberId`_      repeated            
:code:`membership_ids` 2      `MembershipId`_  repeated            
====================== ====== ================ ======== ===========

GetMembersResponse
------------------

======================= ====== ================ ======== ===========
Field                   Number Type             Label    Description
======================= ====== ================ ======== ===========
:code:`members`         1      `Member`_        repeated            
:code:`member_profiles` 2      `MemberProfile`_ repeated            
======================= ====== ================ ======== ===========

UserPresence
------------

==================== ====== ================= ======== ===========
Field                Number Type              Label    Description
==================== ====== ================= ======== ===========
:code:`user_id`      1      `UserId`_         optional            
:code:`presence`     2      `Presence`_       optional            
:code:`active_until` 4      int64             optional            
:code:`dnd_state`    3      `DndState_State`_ optional            
:code:`user_status`  5      `UserStatus`_     optional            
==================== ====== ================= ======== ===========

GetUserPresenceRequest
----------------------

============================ ====== ================ ======== ===========
Field                        Number Type             Label    Description
============================ ====== ================ ======== ===========
:code:`request_header`       100    `RequestHeader`_ optional            
:code:`user_ids`             1      `UserId`_        repeated            
:code:`include_active_until` 2      bool             optional            
:code:`include_user_status`  3      bool             optional            
============================ ====== ================ ======== ===========

GetUserPresenceResponse
-----------------------

====================== ====== =============== ======== ===========
Field                  Number Type            Label    Description
====================== ====== =============== ======== ===========
:code:`user_presences` 1      `UserPresence`_ repeated            
====================== ====== =============== ======== ===========

JAddOnsIdentifier
-----------------

========== ====== ====== ======== ===========
Field      Number Type   Label    Description
========== ====== ====== ======== ===========
:code:`id` 1      string optional            
========== ====== ====== ======== ===========

JAddOnsFormattedText
--------------------

=============================== ====== ============================================ ======== ===========
Field                           Number Type                                         Label    Description
=============================== ====== ============================================ ======== ===========
:code:`id`                      4      `JAddOnsIdentifier`_                         optional            
:code:`original_text`           1      string                                       optional            
:code:`formatted_text_elements` 2      `JAddOnsFormattedText.FormattedTextElement`_ repeated            
:code:`text_align`              3      `JAddOnsFormattedText.TextAlign`_            optional            
=============================== ====== ============================================ ======== ===========

JAddOnsFormattedText.FormattedTextElement
-----------------------------------------

=================== ====== ======================================================= ======== ===============
Field               Number Type                                                    Label    Description    
=================== ====== ======================================================= ======== ===============
:code:`styled_text` 1      `JAddOnsFormattedText.FormattedTextElement.StyledText`_ optional oneof Element {
:code:`hyperlink`   2      `JAddOnsFormattedText.FormattedTextElement.HyperLink`_  optional }              
=================== ====== ======================================================= ======== ===============

JAddOnsFormattedText.FormattedTextElement.DateTime
--------------------------------------------------

================================ ====== ===== ======== ===========
Field                            Number Type  Label    Description
================================ ====== ===== ======== ===========
:code:`time_millis`              1      int64 optional            
:code:`time_zone_offset_minutes` 2      int32 optional            
:code:`date_only`                3      bool  optional            
:code:`floating_time`            4      bool  optional            
================================ ====== ===== ======== ===========

JAddOnsFormattedText.FormattedTextElement.StyledText
----------------------------------------------------

==================== ====== ================================================================== ======== ===========
Field                Number Type                                                               Label    Description
==================== ====== ================================================================== ======== ===========
:code:`text`         1      string                                                             optional            
:code:`datetime`     4      `JAddOnsFormattedText.FormattedTextElement.DateTime`_              optional            
:code:`styles`       2      `JAddOnsFormattedText.FormattedTextElement.StyledText.Style`_      repeated            
:code:`font_weight`  5      `JAddOnsFormattedText.FormattedTextElement.StyledText.FontWeight`_ optional            
:code:`color`        3      int32                                                              optional            
:code:`theme_colors` 7      `JAddOnsThemeColors`_                                              optional            
==================== ====== ================================================================== ======== ===========

JAddOnsFormattedText.FormattedTextElement.StyledText.Style
----------------------------------------------------------

======================= ====== ===========
Name                    Number Description
======================= ====== ===========
:code:`NONE`            0                 
:code:`BOLD_DEPRECATED` 1                 
:code:`ITALIC`          2                 
:code:`UNDERLINE`       3                 
:code:`STRIKETHROUGH`   4                 
:code:`BR`              5                 
:code:`UPPERCASE`       6                 
======================= ====== ===========

JAddOnsFormattedText.FormattedTextElement.StyledText.FontWeight
---------------------------------------------------------------

=============== ====== ===========
Name            Number Description
=============== ====== ===========
:code:`REGULAR` 0                 
:code:`LIGHT`   1                 
:code:`MEDIUM`  2                 
:code:`BOLD`    3                 
=============== ====== ===========

JAddOnsFormattedText.FormattedTextElement.HyperLink
---------------------------------------------------

===================== ====== ====== ======== ===========
Field                 Number Type   Label    Description
===================== ====== ====== ======== ===========
:code:`link`          1      string optional            
:code:`original_link` 3      string optional            
:code:`text`          2      string optional            
===================== ====== ====== ======== ===========

JAddOnsFormattedText.TextAlign
------------------------------

============== ====== ===========
Name           Number Description
============== ====== ===========
:code:`LEFT`   0                 
:code:`CENTER` 1                 
:code:`RIGHT`  2                 
============== ====== ===========

JAddOnsImageCropStyle
---------------------

==================== ====== ====================================== ======== ===========
Field                Number Type                                   Label    Description
==================== ====== ====================================== ======== ===========
:code:`type`         1      `JAddOnsImageCropStyle.ImageCropType`_ optional            
:code:`aspect_ratio` 2      double                                 optional            
==================== ====== ====================================== ======== ===========

JAddOnsImageCropStyle.ImageCropType
-----------------------------------

========================= ====== ===========
Name                      Number Description
========================= ====== ===========
:code:`CROP_TYPE_NOT_SET` 0                 
:code:`SQUARE`            1                 
:code:`CIRCLE`            2                 
:code:`RECTANGLE_CUSTOM`  3                 
:code:`RECTANGLE_4_3`     4                 
========================= ====== ===========

JAddOnsThemeColors
------------------

========================= ====== ===== ======== ===========
Field                     Number Type  Label    Description
========================= ====== ===== ======== ===========
:code:`light_theme_color` 1      int32 optional            
:code:`dark_theme_color`  2      int32 optional            
========================= ====== ===== ======== ===========

JAddOnsOpenLink
---------------

====================== ====== ================================ ======== ===========
Field                  Number Type                             Label    Description
====================== ====== ================================ ======== ===========
:code:`url`            1      string                           optional            
:code:`original_link`  5      string                           optional            
:code:`open_as`        2      `JAddOnsOpenLink.OpenAs`_        optional            
:code:`on_close`       3      `JAddOnsOpenLink.OnClose`_       optional            
:code:`load_indicator` 4      `JAddOnsOpenLink.LoadIndicator`_ optional            
====================== ====== ================================ ======== ===========

JAddOnsOpenLink.OpenAs
----------------------

================= ====== ===========
Name              Number Description
================= ====== ===========
:code:`FULL_SIZE` 0                 
:code:`OVERLAY`   1                 
================= ====== ===========

JAddOnsOpenLink.OnClose
-----------------------

===================== ====== ===========
Name                  Number Description
===================== ====== ===========
:code:`NOTHING`       0                 
:code:`RELOAD_ADD_ON` 1                 
===================== ====== ===========

JAddOnsOpenLink.LoadIndicator
-----------------------------

=============== ====== ===========
Name            Number Description
=============== ====== ===========
:code:`NONE`    0                 
:code:`SPINNER` 1                 
=============== ====== ===========

JAddOnsCardItem
---------------

=============================== ====== ======================================== ======== ===========
Field                           Number Type                                     Label    Description
=============================== ====== ======================================== ======== ===========
:code:`header`                  1      `JAddOnsCardItem.CardItemHeader`_        optional            
:code:`sections`                2      `JAddOnsCardItem.CardItemSection`_       repeated            
:code:`card_actions`            3      `JAddOnsCardItem.CardItemAction`_        repeated            
:code:`name`                    4      string                                   optional            
:code:`fixed_footer`            5      `JAddOnsCardItem.CardItemFixedFooter`_   optional            
:code:`refresh_action`          6      `JAddOnsCardItem.CardItemRefreshAction`_ optional            
:code:`display_style`           7      `JAddOnsCardItem.DisplayStyle`_          optional            
:code:`peek_card_header`        8      `JAddOnsCardItem.CardItemHeader`_        optional            
:code:`background_theme_colors` 9      `JAddOnsThemeColors`_                    optional            
=============================== ====== ======================================== ======== ===========

JAddOnsCardItem.CardItemHeader
------------------------------

====================== ====== ====================================== ======== ===========
Field                  Number Type                                   Label    Description
====================== ====== ====================================== ======== ===========
:code:`title`          1      `JAddOnsFormattedText`_                optional            
:code:`subtitle`       2      `JAddOnsFormattedText`_                optional            
:code:`image_style`    3      `JAddOnsImageCropStyle.ImageCropType`_ optional            
:code:`image_url`      4      string                                 optional            
:code:`image_alt_text` 5      string                                 optional            
====================== ====== ====================================== ======== ===========

JAddOnsCardItem.CardItemSection
-------------------------------

================================= ====== ======================= ======== ===========
Field                             Number Type                    Label    Description
================================= ====== ======================= ======== ===========
:code:`id`                        6      `JAddOnsIdentifier`_    optional            
:code:`description`               1      string                  optional            
:code:`header`                    5      `JAddOnsFormattedText`_ optional            
:code:`widgets`                   2      `JAddOnsWidget`_        repeated            
:code:`collapsable`               3      bool                    optional            
:code:`num_uncollapsable_widgets` 4      int32                   optional            
================================= ====== ======================= ======== ===========

JAddOnsCardItem.CardItemAction
------------------------------

==================== ====== ================= ======== ===========
Field                Number Type              Label    Description
==================== ====== ================= ======== ===========
:code:`action_label` 1      string            optional            
:code:`on_click`     2      `JAddOnsOnClick`_ optional            
==================== ====== ================= ======== ===========

JAddOnsCardItem.CardItemFixedFooter
-----------------------------------

======================== ====== =========================== ======== ===========
Field                    Number Type                        Label    Description
======================== ====== =========================== ======== ===========
:code:`buttons`          1      `JAddOnsWidget.Button`_     repeated            
:code:`primary_button`   2      `JAddOnsWidget.TextButton`_ optional            
:code:`secondary_button` 3      `JAddOnsWidget.TextButton`_ optional            
======================== ====== =========================== ======== ===========

JAddOnsCardItem.CardItemRefreshAction
-------------------------------------

============== ====== ==================== ======== ===========
Field          Number Type                 Label    Description
============== ====== ==================== ======== ===========
:code:`method` 1      `JAddOnsFormAction`_ optional            
============== ====== ==================== ======== ===========

JAddOnsCardItem.DisplayStyle
----------------------------

================================= ====== ===========
Name                              Number Description
================================= ====== ===========
:code:`DISPLAY_STYLE_UNSPECIFIED` 0                 
:code:`PEEK`                      1                 
:code:`REPLACE`                   2                 
================================= ====== ===========

JAddOnsPushCard
---------------

============ ====== ================== ======== ===========
Field        Number Type               Label    Description
============ ====== ================== ======== ===========
:code:`card` 1      `JAddOnsCardItem`_ optional            
============ ====== ================== ======== ===========

JAddOnsOnClick
--------------

======================== ====== ==================== ======== =============================================================================
Field                    Number Type                 Label    Description                                                                  
======================== ====== ==================== ======== =============================================================================
:code:`link`             1      string               optional oneof DataCase {                                                             
:code:`action`           2      `JAddOnsFormAction`_ optional                                                                              
:code:`open_link`        5      `JAddOnsOpenLink`_   optional                                                                              
:code:`open_link_action` 7      `JAddOnsFormAction`_ optional                                                                              
:code:`push_card`        8      `JAddOnsPushCard`_   optional optional        ComGoogleAppsExtensionsV1HostAppAction host_app_action = 9; }
======================== ====== ==================== ======== =============================================================================

JAddOnsTextWidget
-----------------

============ ====== ====== ======== ===========
Field        Number Type   Label    Description
============ ====== ====== ======== ===========
:code:`line` 1      string repeated            
============ ====== ====== ======== ===========

JAddOnsImageComponent
---------------------

==================== ====== ======================== ======== ===========
Field                Number Type                     Label    Description
==================== ====== ======================== ======== ===========
:code:`image_url`    1      string                   optional            
:code:`alt_text`     2      string                   optional            
:code:`crop_style`   3      `JAddOnsImageCropStyle`_ optional            
:code:`border_style` 4      `JAddOnsBorderStyle`_    optional            
==================== ====== ======================== ======== ===========

JAddOnsGrid
-----------

==================== ====== ======================= ======== ===========
Field                Number Type                    Label    Description
==================== ====== ======================= ======== ===========
:code:`id`           1      `JAddOnsIdentifier`_    optional            
:code:`title`        2      string                  optional            
:code:`items`        3      `JAddOnsGrid.GridItem`_ repeated            
:code:`border_style` 4      `JAddOnsBorderStyle`_   optional            
:code:`num_columns`  5      int32                   optional            
:code:`on_click`     6      `JAddOnsOnClick`_       optional            
==================== ====== ======================= ======== ===========

JAddOnsGrid.GridItem
--------------------

====================== ====== ====================================== ======== ===========
Field                  Number Type                                   Label    Description
====================== ====== ====================================== ======== ===========
:code:`identifier`     1      string                                 optional            
:code:`image`          2      `JAddOnsImageComponent`_               optional            
:code:`title`          3      string                                 optional            
:code:`subtitle`       4      string                                 optional            
:code:`text_alignment` 5      `JAddOnsWidget.HorizontalAlign`_       optional            
:code:`layout`         9      `JAddOnsGrid.GridItem.GridItemLayout`_ optional            
:code:`on_click`       10     `JAddOnsOnClick`_                      optional            
====================== ====== ====================================== ======== ===========

JAddOnsGrid.GridItem.GridItemLayout
-----------------------------------

================== ====== ===========
Name               Number Description
================== ====== ===========
:code:`NOT_SET`    0                 
:code:`TEXT_BELOW` 1                 
:code:`TEXT_ABOVE` 2                 
================== ====== ===========

JAddOnsBorderStyle
------------------

===================== ====== ================================ ======== ===========
Field                 Number Type                             Label    Description
===================== ====== ================================ ======== ===========
:code:`type`          1      `JAddOnsBorderStyle.BorderType`_ optional            
:code:`stroke_color`  2      `JAddOnsThemeColors`_            optional            
:code:`corner_radius` 3      int32                            optional            
===================== ====== ================================ ======== ===========

JAddOnsBorderStyle.BorderType
-----------------------------

=========================== ====== ===========
Name                        Number Description
=========================== ====== ===========
:code:`BORDER_TYPE_NOT_SET` 0                 
:code:`NO_BORDER`           1                 
:code:`STROKE`              2                 
=========================== ====== ===========

JAddOnsLabelContentPair
-----------------------

=============== ====== ====== ======== ===========
Field           Number Type   Label    Description
=============== ====== ====== ======== ===========
:code:`label`   1      string optional            
:code:`content` 2      string optional            
=============== ====== ====== ======== ===========

JAddOnsLabelContentPairWidget
-----------------------------

========================== ====== ========================== ======== ===========
Field                      Number Type                       Label    Description
========================== ====== ========================== ======== ===========
:code:`label_content_pair` 1      `JAddOnsLabelContentPair`_ repeated            
========================== ====== ========================== ======== ===========

JAddOnsWidget
-------------

================================= ====== ================================= ======== ============
Field                             Number Type                              Label    Description 
================================= ====== ================================= ======== ============
:code:`text_widget`               1      `JAddOnsTextWidget`_              optional oneof Data {
:code:`label_content_pair_widget` 2      `JAddOnsLabelContentPairWidget`_  optional             
:code:`text_paragraph`            3      `JAddOnsWidget.TextParagraph`_    optional             
:code:`text_key_value`            4      `JAddOnsWidget.TextKeyValue`_     optional             
:code:`image_key_value`           5      `JAddOnsWidget.ImageKeyValue`_    optional             
:code:`image`                     9      `JAddOnsWidget.Image`_            optional             
:code:`key_value`                 13     `JAddOnsWidget.KeyValue`_         optional             
:code:`divider`                   16     `JAddOnsWidget.Divider`_          optional             
:code:`grid`                      17     `JAddOnsGrid`_                    optional             
:code:`menu`                      10     `JAddOnsWidget.Menu`_             optional             
:code:`text_field`                11     `JAddOnsWidget.TextField`_        optional             
:code:`selection_control`         12     `JAddOnsWidget.SelectionControl`_ optional             
:code:`date_time_picker`          14     `JAddOnsWidget.DateTimePicker`_   optional             
:code:`buttons`                   8      `JAddOnsWidget.Button`_           repeated }           
:code:`horizontal_align`          15     `JAddOnsWidget.HorizontalAlign`_  optional             
================================= ====== ================================= ======== ============

JAddOnsWidget.TextParagraph
---------------------------

============ ====== ======================= ======== ===========
Field        Number Type                    Label    Description
============ ====== ======================= ======== ===========
:code:`text` 1      `JAddOnsFormattedText`_ optional            
============ ====== ======================= ======== ===========

JAddOnsWidget.TextKeyValue
--------------------------

================ ====== ======================= ======== ===========
Field            Number Type                    Label    Description
================ ====== ======================= ======== ===========
:code:`key`      1      `JAddOnsFormattedText`_ optional            
:code:`text`     2      `JAddOnsFormattedText`_ optional            
:code:`on_click` 3      `JAddOnsOnClick`_       optional            
================ ====== ======================= ======== ===========

JAddOnsWidget.ImageKeyValue
---------------------------

================ ====== ======================= ======== ===========
Field            Number Type                    Label    Description
================ ====== ======================= ======== ===========
:code:`icon_url` 1      string                  optional            
:code:`text`     2      `JAddOnsFormattedText`_ optional            
:code:`on_click` 3      `JAddOnsOnClick`_       optional            
================ ====== ======================= ======== ===========

JAddOnsWidget.Image
-------------------

====================== ====== ==================== ======== ===========
Field                  Number Type                 Label    Description
====================== ====== ==================== ======== ===========
:code:`id`             5      `JAddOnsIdentifier`_ optional            
:code:`fife_image_url` 1      string               optional            
:code:`on_click`       2      `JAddOnsOnClick`_    optional            
:code:`aspect_ratio`   3      double               optional            
:code:`alt_text`       4      string               optional            
====================== ====== ==================== ======== ===========

JAddOnsWidget.Icon
------------------

=================== ====== ====================================== ======== ===========
Field               Number Type                                   Label    Description
=================== ====== ====================================== ======== ===========
:code:`icon_url`    1      string                                 optional            
:code:`alt_text`    2      string                                 optional            
:code:`image_style` 3      `JAddOnsImageCropStyle.ImageCropType`_ optional            
=================== ====== ====================================== ======== ===========

JAddOnsWidget.KeyValue
----------------------

========================= ====== ====================================== ======== ===============
Field                     Number Type                                   Label    Description    
========================= ====== ====================================== ======== ===============
:code:`icon_url`          1      string                                 optional                
:code:`icon_alt_text`     9      string                                 optional                
:code:`image_style`       10     `JAddOnsImageCropStyle.ImageCropType`_ optional                
:code:`start_icon`        11     `JAddOnsWidget.Icon`_                  optional                
:code:`top_label`         2      `JAddOnsFormattedText`_                optional                
:code:`content`           3      `JAddOnsFormattedText`_                optional                
:code:`content_multiline` 8      bool                                   optional                
:code:`bottom_label`      4      `JAddOnsFormattedText`_                optional                
:code:`on_click`          5      `JAddOnsOnClick`_                      optional                
:code:`button`            6      `JAddOnsWidget.Button`_                optional oneof Control {
:code:`switch_widget`     7      `JAddOnsWidget.KeyValue.SwitchWidget`_ optional                
:code:`end_icon`          12     `JAddOnsWidget.Icon`_                  optional }              
========================= ====== ====================================== ======== ===============

JAddOnsWidget.KeyValue.SwitchWidget
-----------------------------------

==================== ====== ================================================== ======== ===========
Field                Number Type                                               Label    Description
==================== ====== ================================================== ======== ===========
:code:`id`           5      `JAddOnsIdentifier`_                               optional            
:code:`name`         1      string                                             optional            
:code:`value`        2      string                                             optional            
:code:`selected`     3      bool                                               optional            
:code:`on_change`    4      `JAddOnsFormAction`_                               optional            
:code:`control_type` 6      `JAddOnsWidget.KeyValue.SwitchWidget.ControlType`_ optional            
==================== ====== ================================================== ======== ===========

JAddOnsWidget.KeyValue.SwitchWidget.ControlType
-----------------------------------------------

=================== ====== ===========
Name                Number Description
=================== ====== ===========
:code:`UNSPECIFIED` 0                 
:code:`SWITCH`      1                 
:code:`CHECKBOX`    2                 
=================== ====== ===========

JAddOnsWidget.Divider
---------------------

===== ====== ==== ===== ===========
Field Number Type Label Description
===== ====== ==== ===== ===========
===== ====== ==== ===== ===========

JAddOnsWidget.Menu
------------------

================= ====== ============================== ======== ===========
Field             Number Type                           Label    Description
================= ====== ============================== ======== ===========
:code:`name`      1      string                         optional            
:code:`items`     2      `JAddOnsWidget.Menu.MenuItem`_ repeated            
:code:`on_change` 4      `JAddOnsFormAction`_           optional            
:code:`label`     5      string                         optional            
================= ====== ============================== ======== ===========

JAddOnsWidget.Menu.MenuItem
---------------------------

================ ====== ====== ======== ===========
Field            Number Type   Label    Description
================ ====== ====== ======== ===========
:code:`text`     1      string optional            
:code:`value`    4      string optional            
:code:`selected` 3      bool   optional            
================ ====== ====== ======== ===========

JAddOnsWidget.AutoComplete
--------------------------

============= ====== ============================================== ======== ===========
Field         Number Type                                           Label    Description
============= ====== ============================================== ======== ===========
:code:`items` 1      `JAddOnsWidget.AutoComplete.AutoCompleteItem`_ repeated            
============= ====== ============================================== ======== ===========

JAddOnsWidget.AutoComplete.AutoCompleteItem
-------------------------------------------

============ ====== ====== ======== ===========
Field        Number Type   Label    Description
============ ====== ====== ======== ===========
:code:`text` 1      string optional            
============ ====== ====== ======== ===========

JAddOnsWidget.TextField
-----------------------

========================================= ====== ======================================== ======== ===========
Field                                     Number Type                                     Label    Description
========================================= ====== ======================================== ======== ===========
:code:`id`                                11     `JAddOnsIdentifier`_                     optional            
:code:`name`                              1      string                                   optional            
:code:`max_lines`                         2      int32                                    optional            
:code:`type`                              5      `JAddOnsWidget.TextField.TextFieldType`_ optional            
:code:`label`                             3      string                                   optional            
:code:`hint_text`                         4      string                                   optional            
:code:`value`                             6      string                                   optional            
:code:`line_type`                         7      `JAddOnsWidget.TextField.LineType`_      optional            
:code:`on_change`                         8      `JAddOnsFormAction`_                     optional            
:code:`auto_complete`                     9      `JAddOnsWidget.AutoComplete`_            optional            
:code:`auto_complete_callback`            10     `JAddOnsFormAction`_                     optional            
:code:`auto_complete_multiple_selections` 12     bool                                     optional            
========================================= ====== ======================================== ======== ===========

JAddOnsWidget.TextField.TextFieldType
-------------------------------------

================ ====== ===========
Name             Number Description
================ ====== ===========
:code:`REGULAR`  0                 
:code:`FLOATING` 1                 
================ ====== ===========

JAddOnsWidget.TextField.LineType
--------------------------------

================ ====== ===========
Name             Number Description
================ ====== ===========
:code:`SINGLE`   0                 
:code:`MULTIPLE` 1                 
================ ====== ===========

JAddOnsWidget.SelectionControl
------------------------------

================= ====== =============================================== ======== ===========
Field             Number Type                                            Label    Description
================= ====== =============================================== ======== ===========
:code:`id`        6      `JAddOnsIdentifier`_                            optional            
:code:`name`      1      string                                          optional            
:code:`type`      2      `JAddOnsWidget.SelectionControl.SelectionType`_ optional            
:code:`items`     3      `JAddOnsWidget.SelectionControl.SelectionItem`_ repeated            
:code:`on_change` 4      `JAddOnsFormAction`_                            optional            
:code:`label`     5      string                                          optional            
================= ====== =============================================== ======== ===========

JAddOnsWidget.SelectionControl.SelectionItem
--------------------------------------------

================ ====== ==================== ======== ===========
Field            Number Type                 Label    Description
================ ====== ==================== ======== ===========
:code:`id`       4      `JAddOnsIdentifier`_ optional            
:code:`text`     1      string               optional            
:code:`selected` 2      bool                 optional            
:code:`value`    3      string               optional            
================ ====== ==================== ======== ===========

JAddOnsWidget.SelectionControl.SelectionType
--------------------------------------------

==================== ====== ===========
Name                 Number Description
==================== ====== ===========
:code:`CHECK_BOX`    0                 
:code:`RADIO_BUTTON` 1                 
:code:`SWITCH`       2                 
:code:`DROPDOWN`     3                 
==================== ====== ===========

JAddOnsWidget.DateTimePicker
----------------------------

============================ ====== ================================================== ======== ===========
Field                        Number Type                                               Label    Description
============================ ====== ================================================== ======== ===========
:code:`name`                 1      string                                             optional            
:code:`label`                2      string                                             optional            
:code:`type`                 3      `JAddOnsWidget.DateTimePicker.DateTimePickerType`_ optional            
:code:`value_ms_epoch`       4      int64                                              optional            
:code:`timezone_offset_date` 5      int32                                              optional            
:code:`on_change`            6      `JAddOnsFormAction`_                               optional            
:code:`color`                7      int32                                              optional            
:code:`theme_colors`         8      `JAddOnsThemeColors`_                              optional            
============================ ====== ================================================== ======== ===========

JAddOnsWidget.DateTimePicker.DateTimePickerType
-----------------------------------------------

======================== ====== ===========
Name                     Number Description
======================== ====== ===========
:code:`UNSPECIFIED_TYPE` 0                 
:code:`DATE_AND_TIME`    1                 
:code:`DATE_ONLY`        2                 
:code:`TIME_ONLY`        3                 
======================== ====== ===========

JAddOnsWidget.TextButton
------------------------

=============================== ====== ================================= ======== ===========
Field                           Number Type                              Label    Description
=============================== ====== ================================= ======== ===========
:code:`id`                      3      `JAddOnsIdentifier`_              optional            
:code:`text`                    1      `JAddOnsFormattedText`_           optional            
:code:`on_click`                2      `JAddOnsOnClick`_                 optional            
:code:`disabled`                4      bool                              optional            
:code:`style`                   5      `JAddOnsWidget.TextButton.Style`_ optional            
:code:`background_color`        6      int32                             optional            
:code:`background_theme_colors` 7      `JAddOnsThemeColors`_             optional            
:code:`alt_text`                8      string                            optional            
=============================== ====== ================================= ======== ===========

JAddOnsWidget.TextButton.Style
------------------------------

=================== ====== ===========
Name                Number Description
=================== ====== ===========
:code:`UNSPECIFIED` 0                 
:code:`TEXT`        1                 
:code:`FILLED`      2                 
=================== ====== ===========

JAddOnsWidget.ImageButton
-------------------------

================ ====== ==================== ======== ===========
Field            Number Type                 Label    Description
================ ====== ==================== ======== ===========
:code:`id`       4      `JAddOnsIdentifier`_ optional            
:code:`icon_url` 1      string               optional            
:code:`on_click` 2      `JAddOnsOnClick`_    optional            
:code:`name`     3      string               optional            
================ ====== ==================== ======== ===========

JAddOnsWidget.Button
--------------------

==================== ====== ============================ ======== ============
Field                Number Type                         Label    Description 
==================== ====== ============================ ======== ============
:code:`text_button`  1      `JAddOnsWidget.TextButton`_  optional oneof Type {
:code:`image_button` 2      `JAddOnsWidget.ImageButton`_ optional }           
==================== ====== ============================ ======== ============

JAddOnsWidget.HorizontalAlign
-----------------------------

=============== ====== ===========
Name            Number Description
=============== ====== ===========
:code:`NOT_SET` 0                 
:code:`START`   1                 
:code:`CENTER`  2                 
:code:`END`     3                 
=============== ====== ===========

JAddOnsFormAction
-----------------

============================= ====== ==================================== ======== ===========
Field                         Number Type                                 Label    Description
============================= ====== ==================================== ======== ===========
:code:`action_method_name`    1      string                               optional            
:code:`parameters`            4      `JAddOnsFormAction.ActionParameter`_ repeated            
:code:`parameters_deprecated` 3      string                               repeated            
:code:`encrypted_action`      7      string                               optional            
:code:`load_indicator`        5      `JAddOnsFormAction.LoadIndicator`_   optional            
:code:`persist_values`        6      bool                                 optional            
============================= ====== ==================================== ======== ===========

JAddOnsFormAction.ActionParameter
---------------------------------

============= ====== ====== ======== ===========
Field         Number Type   Label    Description
============= ====== ====== ======== ===========
:code:`key`   1      string optional            
:code:`value` 2      string optional            
============= ====== ====== ======== ===========

JAddOnsFormAction.LoadIndicator
-------------------------------

=============== ====== ===========
Name            Number Description
=============== ====== ===========
:code:`SPINNER` 0                 
:code:`NONE`    1                 
=============== ====== ===========

JAddOnsContextualAddOn
----------------------

=============== ====== ================================= ======== ===========
Field           Number Type                              Label    Description
=============== ====== ================================= ======== ===========
:code:`toolbar` 1      `JAddOnsContextualAddOn.Toolbar`_ optional            
:code:`cards`   2      `JAddOnsContextualAddOn.Card`_    repeated            
=============== ====== ================================= ======== ===========

JAddOnsContextualAddOn.Toolbar
------------------------------

==================== ====== ======================= ======== ===========
Field                Number Type                    Label    Description
==================== ====== ======================= ======== ===========
:code:`name`         1      `JAddOnsFormattedText`_ optional            
:code:`icon_url`     2      string                  optional            
:code:`color`        3      int32                   optional            
:code:`theme_colors` 4      `JAddOnsThemeColors`_   optional            
==================== ====== ======================= ======== ===========

JAddOnsContextualAddOn.Card
---------------------------

=============================== ====== ============================================ ======== ===========
Field                           Number Type                                         Label    Description
=============================== ====== ============================================ ======== ===========
:code:`header`                  1      `JAddOnsContextualAddOn.Card.CardHeader`_    optional            
:code:`sections`                2      `JAddOnsContextualAddOn.Card.Section`_       repeated            
:code:`card_actions`            3      `JAddOnsContextualAddOn.Card.CardAction`_    repeated            
:code:`name`                    4      string                                       optional            
:code:`fixed_footer`            5      `JAddOnsContextualAddOn.Card.FixedFooter`_   optional            
:code:`refresh_action`          6      `JAddOnsContextualAddOn.Card.RefreshAction`_ optional            
:code:`background_theme_colors` 7      `JAddOnsThemeColors`_                        optional            
=============================== ====== ============================================ ======== ===========

JAddOnsContextualAddOn.Card.CardHeader
--------------------------------------

====================== ====== ====================================== ======== ===========
Field                  Number Type                                   Label    Description
====================== ====== ====================================== ======== ===========
:code:`title`          1      `JAddOnsFormattedText`_                optional            
:code:`subtitle`       2      `JAddOnsFormattedText`_                optional            
:code:`image_style`    3      `JAddOnsImageCropStyle.ImageCropType`_ optional            
:code:`image_url`      4      string                                 optional            
:code:`image_alt_text` 5      string                                 optional            
====================== ====== ====================================== ======== ===========

JAddOnsContextualAddOn.Card.Section
-----------------------------------

================================= ====== ======================= ======== ===========
Field                             Number Type                    Label    Description
================================= ====== ======================= ======== ===========
:code:`id`                        6      `JAddOnsIdentifier`_    optional            
:code:`description`               1      string                  optional            
:code:`header`                    5      `JAddOnsFormattedText`_ optional            
:code:`widgets`                   2      `JAddOnsWidget`_        repeated            
:code:`collapsable`               3      bool                    optional            
:code:`num_uncollapsable_widgets` 4      int32                   optional            
================================= ====== ======================= ======== ===========

JAddOnsContextualAddOn.Card.CardAction
--------------------------------------

==================== ====== ================= ======== ===========
Field                Number Type              Label    Description
==================== ====== ================= ======== ===========
:code:`action_label` 1      string            optional            
:code:`on_click`     2      `JAddOnsOnClick`_ optional            
==================== ====== ================= ======== ===========

JAddOnsContextualAddOn.Card.FixedFooter
---------------------------------------

======================== ====== =========================== ======== ===========
Field                    Number Type                        Label    Description
======================== ====== =========================== ======== ===========
:code:`buttons`          1      `JAddOnsWidget.Button`_     repeated            
:code:`primary_button`   2      `JAddOnsWidget.TextButton`_ optional            
:code:`secondary_button` 3      `JAddOnsWidget.TextButton`_ optional            
======================== ====== =========================== ======== ===========

JAddOnsContextualAddOn.Card.RefreshAction
-----------------------------------------

============== ====== ==================== ======== ===========
Field          Number Type                 Label    Description
============== ====== ==================== ======== ===========
:code:`method` 1      `JAddOnsFormAction`_ optional            
============== ====== ==================== ======== ===========

Html
----

============ ====== ====== ======== =====================================================
Field        Number Type   Label    Description                                          
============ ====== ====== ======== =====================================================
:code:`html` 2      string optional private_do_not_access_or_else_safe_html_wrapped_value
============ ====== ====== ======== =====================================================

HtmlAttachment
--------------

============ ====== ======= ======== ===========
Field        Number Type    Label    Description
============ ====== ======= ======== ===========
:code:`html` 2      `Html`_ optional            
============ ====== ======= ======== ===========

Attachment
----------

=================== ====== ========================= ======== ============
Field               Number Type                      Label    Description 
=================== ====== ========================= ======== ============
:code:`html`        1      `HtmlAttachment`_         optional oneof Type {
:code:`add_on_data` 2      `JAddOnsContextualAddOn`_ optional             
:code:`app_id`      5      `UserId`_                 optional }           
:code:`app_user`    6      `User`_                   optional             
=================== ====== ========================= ======== ============

Reaction
--------

================================= ====== ======== ======== ===========
Field                             Number Type     Label    Description
================================= ====== ======== ======== ===========
:code:`emoji`                     1      `Emoji`_ optional            
:code:`count`                     2      int32    optional            
:code:`current_user_participated` 3      bool     optional            
:code:`create_timestamp`          4      int64    optional            
================================= ====== ======== ======== ===========

MessageParentId
---------------

================ ====== ========== ======== ==============
Field            Number Type       Label    Description   
================ ====== ========== ======== ==============
:code:`topic_id` 4      `TopicId`_ optional oneof Parent {
================ ====== ========== ======== ==============

MessageId
---------

================== ====== ================== ======== ===========
Field              Number Type               Label    Description
================== ====== ================== ======== ===========
:code:`parent_id`  1      `MessageParentId`_ optional            
:code:`message_id` 2      string             optional            
================== ====== ================== ======== ===========

Message
-------

========================== ====== ======================= ======== ========================================================================================================================================================================================================================================================================================
Field                      Number Type                    Label    Description                                                                                                                                                                                                                                                                             
========================== ====== ======================= ======== ========================================================================================================================================================================================================================================================================================
:code:`id`                 1      `MessageId`_            optional                                                                                                                                                                                                                                                                                         
:code:`message_state`      20     `Message.MessageState`_ optional                                                                                                                                                                                                                                                                                         
:code:`creator`            2      `User`_                 optional                                                                                                                                                                                                                                                                                         
:code:`create_time`        3      int64                   optional                                                                                                                                                                                                                                                                                         
:code:`last_update_time`   4      int64                   optional                                                                                                                                                                                                                                                                                         
:code:`delete_time`        8      int64                   optional                                                                                                                                                                                                                                                                                         
:code:`last_edit_time`     17     int64                   optional                                                                                                                                                                                                                                                                                         
:code:`text_body`          10     string                  optional                                                                                                                                                                                                                                                                                         
:code:`annotations`        11     `Annotation`_           repeated                                                                                                                                                                                                                                                                                         
:code:`local_id`           14     string                  optional repeated DYNProtoBotResponse bot_responses = 19;                                                                                                                                                                                                                                        
:code:`attachments`        15     `Attachment`_           repeated                                                                                                                                                                                                                                                                                         
:code:`retention_settings` 18     `RetentionSettings`_    optional optional DYNProtoAppProfile app_profile = 16;                                                                                                                                                                                                                                           
:code:`reactions`          21     `Reaction`_             repeated                                                                                                                                                                                                                                                                                         
:code:`last_reply_time`    5      int64                   optional repeated DYNProtoCommunalLabelTag communal_labels = 27; repeated DYNProtoPersonalLabelTag personal_labels = 26; optional ?? message_integration_payload = 23; optional DYNProtoMessage_MessagePermission editable_by = 24; optional DYNProtoMessage_MessagePermission deletable_by = 25;
:code:`last_view_time`     6      int64                   optional                                                                                                                                                                                                                                                                                         
:code:`last_mention_time`  7      int64                   optional                                                                                                                                                                                                                                                                                         
:code:`num_unread_replies` 9      int64                   optional                                                                                                                                                                                                                                                                                         
:code:`last_reply`         12     `Message`_              optional                                                                                                                                                                                                                                                                                         
:code:`sort_time`          13     int64                   optional                                                                                                                                                                                                                                                                                         
:code:`message_type`       28     `Message.MessageType`_  optional                                                                                                                                                                                                                                                                                         
:code:`creator_membership` 30     `Membership`_           optional optional DYNProtoAppId origin_app_id = 29;                                                                                                                                                                                                                                              
========================== ====== ======================= ======== ========================================================================================================================================================================================================================================================================================

Message.MessageState
--------------------

===================== ====== ===========
Name                  Number Description
===================== ====== ===========
:code:`UNKNOWN`       0                 
:code:`GROUP_VISIBLE` 1                 
:code:`ON_HOLD`       2                 
===================== ====== ===========

Message.MessageType
-------------------

================================ ====== ===========
Name                             Number Description
================================ ====== ===========
:code:`MESSAGE_TYPE_UNSPECIFIED` 0                 
:code:`USER_MESSAGE`             1                 
:code:`SYSTEM_MESSAGE`           2                 
================================ ====== ===========

DriveMetadata
-------------

================================= ====== ===================== ======== ===================================
Field                             Number Type                  Label    Description                        
================================= ====== ===================== ======== ===================================
:code:`id`                        1      string                optional                                    
:code:`title`                     2      string                optional                                    
:code:`thumbnail_url`             3      string                optional                                    
:code:`thumbnail_width`           7      int32                 optional                                    
:code:`thumbnail_height`          8      int32                 optional                                    
:code:`mimetype`                  4      string                optional                                    
:code:`external_mimetype`         14     string                optional                                    
:code:`organization_display_name` 13     string                optional                                    
:code:`encrypted_doc_id`          10     bool                  optional                                    
:code:`url_fragment`              16     string                optional                                    
:code:`embed_url`                 18     `TrustedResourceUrl`_ optional                                    
:code:`is_owner`                  19     bool                  optional                                    
:code:`can_share`                 5      bool                  optional                                    
:code:`can_edit`                  15     bool                  optional                                    
:code:`can_view`                  6      bool                  optional                                    
:code:`should_not_render`         9      bool                  optional optional DriveState drive_state = ;
:code:`is_download_restricted`    17     bool                  optional                                    
================================= ====== ===================== ======== ===================================

Url
---

ComGoogleCommonHtmlTypesSafeUrlProto

=========== ====== ====== ======== ====================================================
Field       Number Type   Label    Description                                         
=========== ====== ====== ======== ====================================================
:code:`url` 3      string optional private_do_not_access_or_else_safe_url_wrapped_value
=========== ====== ====== ======== ====================================================

TrustedResourceUrl
------------------

ComGoogleCommonHtmlTypesTrustedResourceUrlProto

==================== ====== ====== ======== ================================================================
Field                Number Type   Label    Description                                                     
==================== ====== ====== ======== ================================================================
:code:`resource_url` 4      string optional private_do_not_access_or_else_trusted_resource_url_wrapped_value
==================== ====== ====== ======== ================================================================

UrlMetadata
-----------

==================================== ====== ====== ======== ===========
Field                                Number Type   Label    Description
==================================== ====== ====== ======== ===========
:code:`title`                        1      string optional            
:code:`snippet`                      2      string optional            
:code:`image_url`                    3      string optional            
:code:`image_height`                 4      string optional            
:code:`image_width`                  5      string optional            
:code:`url`                          7      `Url`_ optional            
:code:`gws_url`                      13     `Url`_ optional            
:code:`redirect_url`                 14     `Url`_ optional            
:code:`gws_url_expiration_timestamp` 15     int64  optional            
:code:`domain`                       8      string optional            
:code:`should_not_render`            9      bool   optional            
:code:`int_image_height`             10     int32  optional            
:code:`int_image_width`              11     int32  optional            
:code:`mime_type`                    12     string optional            
==================================== ====== ====== ======== ===========

UploadMetadata
--------------

======================== ====== ====== ======== ============================================================================================
Field                    Number Type   Label    Description                                                                                 
======================== ====== ====== ======== ============================================================================================
:code:`attachment_token` 1      string optional oneof Payload {                                                                             
:code:`content_name`     3      string optional optional BackendUploadMetadata backend_upload_metadata = 2;  }                              
:code:`content_type`     4      string optional                                                                                             
:code:`local_id`         6      string optional optional Dimension original_dimension = 5;                                                  
:code:`cloned_drive_id`  9      string optional optional VideoReference video_reference = 7; optional VirusScanResult virus_scan_result = 8;
======================== ====== ====== ======== ============================================================================================

FormatMetadata
--------------

=================== ====== ============================ ======== ===========
Field               Number Type                         Label    Description
=================== ====== ============================ ======== ===========
:code:`format_type` 1      `FormatMetadata.FormatType`_ optional            
:code:`font_color`  2      int32                        optional            
=================== ====== ============================ ======== ===========

FormatMetadata.FormatType
-------------------------

========================== ====== ===========
Name                       Number Description
========================== ====== ===========
:code:`TYPE_UNSPECIFIED`   0                 
:code:`BOLD`               1                 
:code:`ITALIC`             2                 
:code:`STRIKE`             3                 
:code:`SOURCE_CODE`        4                 
:code:`MONOSPACE`          5                 
:code:`HIDDEN`             6                 
:code:`MONOSPACE_BLOCK`    7                 
:code:`UNDERLINE`          8                 
:code:`FONT_COLOR`         9                 
:code:`BULLETED_LIST`      10                
:code:`BULLETED_LIST_ITEM` 11                
:code:`CLIENT_HIDDEN`      12                
========================== ====== ===========

Annotation
----------

============================== ====== ============================ ======== ===================================================
Field                          Number Type                         Label    Description                                        
============================== ====== ============================ ======== ===================================================
:code:`type`                   1      `AnnotationType`_            optional                                                    
:code:`start_index`            2      int32                        optional                                                    
:code:`length`                 3      int32                        optional                                                    
:code:`local_id`               9      string                       optional                                                    
:code:`unique_id`              19     string                       optional                                                    
:code:`chip_render_type`       20     `Annotation.ChipRenderType`_ optional                                                    
:code:`server_invalidated`     13     bool                         optional                                                    
:code:`user_mention_metadata`  5      `UserMentionMetadata`_       optional oneof Metadata {                                   
:code:`format_metadata`        8      `FormatMetadata`_            optional                                                    
:code:`slash_command_metadata` 15     `SlashCommandMetadata`_      optional                                                    
:code:`drive_metadata`         4      `DriveMetadata`_             optional                                                    
:code:`url_metadata`           7      `UrlMetadata`_               optional DYNProtoYoutubeMetadata youtube_metadata = 6;      
:code:`upload_metadata`        10     `UploadMetadata`_            optional                                                    
:code:`membership_changed`     11     `MembershipChangedMetadata`_ optional                                                    
:code:`room_updated`           14     `RoomUpdatedMetadata`_       optional DYNProtoVideoCallMetadata video_call_metadata = 12;
============================== ====== ============================ ======== ===================================================

Annotation.ChipRenderType
-------------------------

========================== ====== ===========
Name                       Number Description
========================== ====== ===========
:code:`UNKNOWN`            0                 
:code:`RENDER`             1                 
:code:`RENDER_IF_POSSIBLE` 2                 
:code:`DO_NOT_RENDER`      3                 
========================== ====== ===========

TypingContext
-------------

================ ====== ========== ======== ===============
Field            Number Type       Label    Description    
================ ====== ========== ======== ===============
:code:`group_id` 1      `GroupId`_ optional oneof Context {
:code:`topic_id` 2      `TopicId`_ optional }              
================ ====== ========== ======== ===============

SetTypingStateRequest
---------------------

====================== ====== ================ ======== ===========
Field                  Number Type             Label    Description
====================== ====== ================ ======== ===========
:code:`request_header` 100    `RequestHeader`_ optional            
:code:`state`          1      `TypingState`_   optional            
:code:`context`        2      `TypingContext`_ optional            
====================== ====== ================ ======== ===========

SetTypingStateResponse
----------------------

============================ ====== ===== ======== ===========
Field                        Number Type  Label    Description
============================ ====== ===== ======== ===========
:code:`start_timestamp_usec` 1      int64 optional            
============================ ====== ===== ======== ===========

DmId
----

============= ====== ====== ======== ===========
Field         Number Type   Label    Description
============= ====== ====== ======== ===========
:code:`dm_id` 1      string optional            
============= ====== ====== ======== ===========

SpaceId
-------

================ ====== ====== ======== ===========
Field            Number Type   Label    Description
================ ====== ====== ======== ===========
:code:`space_id` 1      string optional            
================ ====== ====== ======== ===========

GroupId
-------

================ ====== ========== ======== ===========
Field            Number Type       Label    Description
================ ====== ========== ======== ===========
:code:`space_id` 1      `SpaceId`_ optional oneof Id { 
:code:`dm_id`    3      `DmId`_    optional }          
================ ====== ========== ======== ===========

Group
-----

==================================== ====== ================================== ======== =====================================================================================================================================================================
Field                                Number Type                               Label    Description                                                                                                                                                          
==================================== ====== ================================== ======== =====================================================================================================================================================================
:code:`group_id`                     1      `GroupId`_                         optional                                                                                                                                                                      
:code:`name`                         2      string                             optional                                                                                                                                                                      
:code:`creator`                      4      `User`_                            optional                                                                                                                                                                      
:code:`create_time`                  5      int64                              optional                                                                                                                                                                      
:code:`last_modified_time`           6      int64                              optional                                                                                                                                                                      
:code:`sort_time`                    7      int64                              optional                                                                                                                                                                      
:code:`revision_time`                13     int64                              optional                                                                                                                                                                      
:code:`group_read_state`             12     `GroupReadState`_                  optional                                                                                                                                                                      
:code:`retention_horizon_time`       14     int64                              optional                                                                                                                                                                      
:code:`retention_settings`           16     `RetentionSettings`_               optional optional DYNProtoDasherDomainPolicies dasher_domain_policies = 15; optional ?? group_policies = 40;                                                                  
:code:`is_flat`                      17     bool                               optional                                                                                                                                                                      
:code:`interop_enabled`              18     bool                               optional                                                                                                                                                                      
:code:`retention_duration_seconds`   19     int64                              optional optional DYNProtoGroupGuestAccessSettings group_guest_access_settings = 20; optional DYNProtoOrganizationInfo organization_info = 21;                                
:code:`group_type`                   22     `Group.GroupType`_                 optional                                                                                                                                                                      
:code:`visibility`                   23     `GroupVisibility`_                 optional                                                                                                                                                                      
:code:`roster_email`                 25     string                             optional repeated DYNProtoGsuiteIntegration gsuite_integration = 24;                                                                                                          
:code:`flat_group`                   26     `Group.FlatGroup`_                 optional oneof ThreadedModel {                                                                                                                                                
:code:`threaded_group`               27     `Group.ThreadedGroup`_             optional }                                                                                                                                                                    
:code:`avatar_url`                   28     string                             optional                                                                                                                                                                      
:code:`attribute_checker_group_type` 33     `SharedAttributeCheckerGroupType`_ optional optional DYNProtoGroupIntegrationSettings group_integration_settings = 36; repeated ?? space_integration_payloads = 29; optional DYNProtoAvatarInfo avatar_info = 30;
:code:`name_users`                   34     `NameUsers`_                       optional                                                                                                                                                                      
:code:`group_support_level`          38     `GroupSupportLevel`_               optional optional ?? space_details = 37;                                                                                                                                      
:code:`group_unsupported_reason`     39     `GroupUnsupportedReason`_          optional                                                                                                                                                                      
:code:`typing_indicators_enabled`    43     bool                               optional optional DYNProtoHubGroupProperties hub_group_properties = 41; optional ?? ufr_upgrade_info = 42;                                                                    
==================================== ====== ================================== ======== =====================================================================================================================================================================

Group.FlatGroup
---------------

===== ====== ==== ===== ===========
Field Number Type Label Description
===== ====== ==== ===== ===========
===== ====== ==== ===== ===========

Group.ThreadedGroup
-------------------

===== ====== ==== ===== ===========
Field Number Type Label Description
===== ====== ==== ===== ===========
===== ====== ==== ===== ===========

Group.GroupType
---------------

============================== ====== ===========
Name                           Number Description
============================== ====== ===========
:code:`GROUP_TYPE_UNSPECIFIED` 0                 
:code:`ROOM`                   1                 
:code:`HUMAN_DM`               2                 
:code:`BOT_DM`                 3                 
============================== ====== ===========

GroupReadStateId
----------------

================ ====== ========== ======== ===========
Field            Number Type       Label    Description
================ ====== ========== ======== ===========
:code:`user_id`  1      `UserId`_  optional            
:code:`group_id` 2      `GroupId`_ optional            
================ ====== ========== ======== ===========

InviteState
-----------

============================= ====== ============= ======== ===========
Field                         Number Type          Label    Description
============================= ====== ============= ======== ===========
:code:`inviter_user_id`       1      `UserId`_     optional            
:code:`invitation_message_id` 2      `MessageId`_  optional            
:code:`show_welcome_mat`      3      bool          optional            
:code:`invite_type`           5      `InviteType`_ optional            
============================= ====== ============= ======== ===========

GroupReadState
--------------

=========================================================== ====== ============================ ======== ===================================================
Field                                                       Number Type                         Label    Description                                        
=========================================================== ====== ============================ ======== ===================================================
:code:`id`                                                  1      `GroupReadStateId`_          optional                                                    
:code:`last_read_time`                                      2      int64                        optional                                                    
:code:`unread_message_count`                                4      int64                        optional                                                    
:code:`starred`                                             5      bool                         optional                                                    
:code:`update_timestamp`                                    6      int64                        optional                                                    
:code:`unread_subscribed_topic_count`                       7      int64                        optional                                                    
:code:`unread_subscribed_topic_count_consistency_timestamp` 11     int64                        optional                                                    
:code:`unread_subscribed_topics`                            8      `TopicId`_                   repeated                                                    
:code:`hide_timestamp`                                      9      int64                        optional                                                    
:code:`clear_history_timestamp`                             14     int64                        optional                                                    
:code:`invite_state`                                        10     `InviteState`_               optional                                                    
:code:`notification_settings`                               12     `GroupNotificationSettings`_ optional                                                    
:code:`retention_settings`                                  13     `RetentionSettings`_         optional                                                    
:code:`blocked`                                             15     bool                         optional                                                    
:code:`membership_role`                                     22     `MembershipRole`_            optional                                                    
:code:`membership_state`                                    16     `MembershipState`_           optional                                                    
:code:`invite_category`                                     17     `InviteCategory`_            optional                                                    
:code:`mark_as_unread_timestamp_usec`                       18     int64                        optional                                                    
:code:`visible_in_world_view`                               19     bool                         optional                                                    
:code:`show_notification_card_in_stream`                    20     bool                         optional                                                    
:code:`joined_users`                                        23     `UserId`_                    repeated optional NotificationsCard notifications_card = 21;
=========================================================== ====== ============================ ======== ===================================================

GroupNotificationSettings
-------------------------

================== ====== =================================================== ======== ===========
Field              Number Type                                                Label    Description
================== ====== =================================================== ======== ===========
:code:`state`      1      `GroupNotificationSettings.GroupNotificationState`_ optional            
:code:`room_state` 2      `GroupNotificationSettings.RoomNotificationState`_  optional            
================== ====== =================================================== ======== ===========

GroupNotificationSettings.GroupNotificationState
------------------------------------------------

================================== ====== ===========
Name                               Number Description
================================== ====== ===========
:code:`UNKNOWN_NOTIFICATION_STATE` 0                 
:code:`MUTED`                      1                 
:code:`UNMUTED`                    2                 
================================== ====== ===========

GroupNotificationSettings.RoomNotificationState
-----------------------------------------------

==================================== ====== ===========
Name                                 Number Description
==================================== ====== ===========
:code:`NOTIFY_ALWAYS`                0                 
:code:`NOTIFY_LESS_WITH_NEW_THREADS` 1                 
:code:`NOTIFY_LESS`                  2                 
:code:`NOTIFY_NEVER`                 3                 
==================================== ====== ===========

RetentionSettings
-----------------

======================== ====== =================================== ======== ===========
Field                    Number Type                                Label    Description
======================== ====== =================================== ======== ===========
:code:`state`            1      `RetentionSettings.RetentionState`_ optional            
:code:`expiry_timestamp` 2      int64                               optional            
======================== ====== =================================== ======== ===========

RetentionSettings.RetentionState
--------------------------------

=============================== ====== ===========
Name                            Number Description
=============================== ====== ===========
:code:`UNKNOWN_RETENTION_STATE` 0                 
:code:`PERMANENT`               1                 
:code:`EPHEMERAL_ONE_DAY`       2                 
=============================== ====== ===========

GroupData
---------

======================= ====== =================================== ======== ===========
Field                   Number Type                                Label    Description
======================= ====== =================================== ======== ===========
:code:`retention_state` 1      `RetentionSettings.RetentionState`_ optional            
======================= ====== =================================== ======== ===========

TopicId
-------

================ ====== ========== ======== ===========
Field            Number Type       Label    Description
================ ====== ========== ======== ===========
:code:`group_id` 3      `GroupId`_ optional            
:code:`topic_id` 2      string     optional            
================ ====== ========== ======== ===========

Topic
-----

==================================== ====== ==================== ======== ======================================================
Field                                Number Type                 Label    Description                                           
==================================== ====== ==================== ======== ======================================================
:code:`id`                           1      `TopicId`_           optional                                                       
:code:`sort_time`                    2      int64                optional                                                       
:code:`create_time_usec`             15     int64                optional                                                       
:code:`replies`                      7      `Message`_           repeated                                                       
:code:`is_system_message`            12     bool                 optional optional DYNProtoTopicReadState topic_read_state = 11;
:code:`retention_settings`           13     `RetentionSettings`_ optional                                                       
:code:`contains_more_unread_replies` 14     bool                 optional                                                       
==================================== ====== ==================== ======== ======================================================

MessageInfo
-----------

================================= ====== ==== ======== ===========
Field                             Number Type Label    Description
================================= ====== ==== ======== ===========
:code:`accept_format_annotations` 1      bool optional            
================================= ====== ==== ======== ===========

CreateTopicRequest
------------------

============================ ====== ==================== ======== ===========
Field                        Number Type                 Label    Description
============================ ====== ==================== ======== ===========
:code:`request_header`       100    `RequestHeader`_     optional            
:code:`group_id`             5      `GroupId`_           optional            
:code:`text_body`            2      string               optional            
:code:`annotations`          3      `Annotation`_        repeated            
:code:`retention_settings`   6      `RetentionSettings`_ optional            
:code:`local_id`             4      string               optional            
:code:`topic_and_message_id` 7      string               optional            
:code:`history_v2`           8      bool                 optional            
:code:`message_info`         9      `MessageInfo`_       optional            
============================ ====== ==================== ======== ===========

CreateTopicResponse
-------------------

============================== ====== ================ ======== ===========
Field                          Number Type             Label    Description
============================== ====== ================ ======== ===========
:code:`topic`                  1      `Topic`_         optional            
:code:`group_revision`         2      `WriteRevision`_ optional            
:code:`current_group_revision` 3      `ReadRevision`_  optional            
============================== ====== ================ ======== ===========

CreateMessageRequest
--------------------

====================== ====== ================== ======== ===========
Field                  Number Type               Label    Description
====================== ====== ================== ======== ===========
:code:`request_header` 100    `RequestHeader`_   optional            
:code:`parent_id`      1      `MessageParentId`_ optional            
:code:`text_body`      2      string             optional            
:code:`annotations`    3      `Annotation`_      repeated            
:code:`local_id`       4      string             optional            
:code:`message_id`     6      string             optional            
====================== ====== ================== ======== ===========

CreateMessageResponse
---------------------

============================== ====== ================ ======== ===========
Field                          Number Type             Label    Description
============================== ====== ================ ======== ===========
:code:`message`                1      `Message`_       optional            
:code:`group_revision`         3      `WriteRevision`_ optional            
:code:`current_group_revision` 4      `ReadRevision`_  optional            
============================== ====== ================ ======== ===========

InviteeInfo
-----------

=============== ====== ========= ======== ===========
Field           Number Type      Label    Description
=============== ====== ========= ======== ===========
:code:`user_id` 1      `UserId`_ optional            
:code:`email`   2      string    optional            
=============== ====== ========= ======== ===========

InviteeMemberInfo
-----------------

==================== ====== ============== ======== ===========
Field                Number Type           Label    Description
==================== ====== ============== ======== ===========
:code:`invitee_info` 1      `InviteeInfo`_ optional oneof Id { 
==================== ====== ============== ======== ===========

SpaceCreationInfo
-----------------

==================================== ====== ================================== ======== ========================================================================================================
Field                                Number Type                               Label    Description                                                                                             
==================================== ====== ================================== ======== ========================================================================================================
:code:`name`                         1      string                             optional                                                                                                         
:code:`visibility`                   4      `GroupVisibility`_                 optional optional GroupGuestAccessSettings group_guest_access_settings = 3;                                      
:code:`flat_group`                   5      `SpaceCreationInfo.FlatGroup`_     optional                                                                                                         
:code:`threaded_group`               6      `SpaceCreationInfo.ThreadedGroup`_ optional                                                                                                         
:code:`has_server_generated_name`    7      bool                               optional                                                                                                         
:code:`invitee_member_infos`         8      `InviteeMemberInfo`_               repeated optional RoomInviteNotificationSettings notification_settings = 9; optional AvatarInfo avatar_info = 10;
:code:`space_type`                   11     `SpaceCreationInfo.SpaceType`_     optional                                                                                                         
:code:`attribute_checker_group_type` 13     `SharedAttributeCheckerGroupType`_ optional optional ?? space_details = 12;                                                                         
:code:`shared_drive_name`            14     string                             optional                                                                                                         
==================================== ====== ================================== ======== ========================================================================================================

SpaceCreationInfo.FlatGroup
---------------------------

===== ====== ==== ===== ===========
Field Number Type Label Description
===== ====== ==== ===== ===========
===== ====== ==== ===== ===========

SpaceCreationInfo.ThreadedGroup
-------------------------------

===== ====== ==== ===== ===========
Field Number Type Label Description
===== ====== ==== ===== ===========
===== ====== ==== ===== ===========

SpaceCreationInfo.SpaceType
---------------------------

===== ====== ==== ===== ===========
Field Number Type Label Description
===== ====== ==== ===== ===========
===== ====== ==== ===== ===========

CreateGroupRequest
------------------

================================== ====== ==================== ======== ====================
Field                              Number Type                 Label    Description         
================================== ====== ==================== ======== ====================
:code:`request_header`             100    `RequestHeader`_     optional                     
:code:`space`                      1      `SpaceCreationInfo`_ optional oneof CreationInfo {
:code:`local_id`                   3      string               optional }                   
:code:`should_find_existing_space` 4      bool                 optional                     
================================== ====== ==================== ======== ====================

CreateMembershipResult
----------------------

================== ====== ============= ======== ========================================================================================
Field              Number Type          Label    Description                                                                             
================== ====== ============= ======== ========================================================================================
:code:`membership` 1      `Membership`_ optional optional FailureReason reason = 2; repeated SupplementalReason supplemental_reasons = 3;
================== ====== ============= ======== ========================================================================================

CreateGroupResponse
-------------------

====================== ====== ========================= ======== ================================================
Field                  Number Type                      Label    Description                                     
====================== ====== ========================= ======== ================================================
:code:`group`          1      `Group`_                  optional                                                 
:code:`group_revision` 2      `WriteRevision`_          optional                                                 
:code:`user_revision`  3      `WriteRevision`_          optional                                                 
:code:`results`        4      `CreateMembershipResult`_ repeated optional Failure creator_membership_failure = 5;
====================== ====== ========================= ======== ================================================

CreateDmRequest
---------------

============================ ====== =============================== ======== =======================================================
Field                        Number Type                            Label    Description                                            
============================ ====== =============================== ======== =======================================================
:code:`request_header`       100    `RequestHeader`_                optional                                                        
:code:`fetch_options`        8      `CreateDmRequest.FetchOptions`_ repeated                                                        
:code:`members`              1      `UserId`_                       repeated                                                        
:code:`invitees`             7      `InviteeInfo`_                  repeated                                                        
:code:`retention_settings`   3      `RetentionSettings`_            optional optional MessageCreationInfo message_creation_info = 2;
:code:`local_id`             4      string                          optional                                                        
:code:`topic_and_message_id` 5      string                          optional                                                        
============================ ====== =============================== ======== =======================================================

CreateDmRequest.FetchOptions
----------------------------

=========================================== ====== ===========
Name                                        Number Description
=========================================== ====== ===========
:code:`UNKNOWN`                             0                 
:code:`INCLUDE_BOT_MEMBERSHIPS_IN_RESPONSE` 1                 
=========================================== ====== ===========

CreateDmResponse
----------------

====================== ====== ================ ======== ===========
Field                  Number Type             Label    Description
====================== ====== ================ ======== ===========
:code:`dm`             1      `Group`_         optional            
:code:`topic`          2      `Topic`_         optional            
:code:`group_revision` 3      `WriteRevision`_ optional            
:code:`memberships`    4      `Membership`_    repeated            
====================== ====== ================ ======== ===========

ListTopicsResponse
------------------

============================ ====== ================= ======== ===========
Field                        Number Type              Label    Description
============================ ====== ================= ======== ===========
:code:`topics`               1      `Topic`_          repeated            
:code:`user_revision`        2      `ReadRevision`_   optional            
:code:`group_revision`       3      `ReadRevision`_   optional            
:code:`contains_first_topic` 4      bool              optional            
:code:`contains_last_topic`  5      bool              optional            
:code:`read_receipt_set`     6      `ReadReceiptSet`_ optional            
============================ ====== ================= ======== ===========

ListTopicsRequest
-----------------

==================================== ====== ================================= ======== ===============================================================
Field                                Number Type                              Label    Description                                                    
==================================== ====== ================================= ======== ===============================================================
:code:`request_header`               100    `RequestHeader`_                  optional                                                                
:code:`group_id`                     8      `GroupId`_                        optional                                                                
:code:`page_size_for_topics`         2      int32                             optional                                                                
:code:`page_size_for_unread_replies` 6      int32                             optional                                                                
:code:`page_size_for_read_replies`   7      int32                             optional                                                                
:code:`page_size_for_replies`        3      int32                             optional optional DYNProtoListTopicsRequest_Filter filter = 4;          
:code:`fetch_options`                5      `ListTopicsRequest.FetchOptions`_ repeated                                                                
:code:`user_not_older_than`          9      `ReferenceRevision`_              optional                                                                
:code:`group_not_older_than`         10     `ReferenceRevision`_              optional optional DYNProtoListTopicsRequest_SortOption sort_option = 11;
==================================== ====== ================================= ======== ===============================================================

ListTopicsRequest.FetchOptions
------------------------------

============================ ====== ===========
Name                         Number Description
============================ ====== ===========
:code:`USER`                 1                 
:code:`TOTAL_MESSAGE_COUNTS` 2                 
:code:`READ_RECEIPTS`        3                 
============================ ====== ===========

ListMembersRequest
------------------

====================== ====== ==================== ======== ====================================================================
Field                  Number Type                 Label    Description                                                         
====================== ====== ==================== ======== ====================================================================
:code:`request_header` 100    `RequestHeader`_     optional                                                                     
:code:`space_id`       1      `SpaceId`_           optional                                                                     
:code:`group_id`       5      `GroupId`_           optional                                                                     
:code:`page_size`      3      int32                optional repeated FetchOptions fetch_options = 2; optional Filter filter = 7;
:code:`page_token`     4      string               optional                                                                     
:code:`not_older_than` 6      `ReferenceRevision`_ optional                                                                     
====================== ====== ==================== ======== ====================================================================

ListMembersResponse
-------------------

========================== ====== =============== ======== ===========
Field                      Number Type            Label    Description
========================== ====== =============== ======== ===========
:code:`memberships`        1      `Membership`_   repeated            
:code:`members`            2      `Member`_       repeated            
:code:`member_ids`         5      `MemberId`_     repeated            
:code:`next_page_token`    3      string          optional            
:code:`group_revision`     4      `ReadRevision`_ optional            
:code:`invited_member_ids` 6      `MemberId`_     repeated            
========================== ====== =============== ======== ===========

ReadReceipt
-----------

======================== ====== ======= ======== ===========
Field                    Number Type    Label    Description
======================== ====== ======= ======== ===========
:code:`read_time_micros` 2      int64   optional            
:code:`user`             3      `User`_ optional            
======================== ====== ======= ======== ===========

ReadReceiptSet
--------------

===================== ====== ============== ======== ===========
Field                 Number Type           Label    Description
===================== ====== ============== ======== ===========
:code:`enabled`       1      bool           optional            
:code:`read_receipts` 2      `ReadReceipt`_ repeated            
===================== ====== ============== ======== ===========

WebPushNotification
-------------------

========================= ====== ==================== ======== ===========
Field                     Number Type                 Label    Description
========================= ====== ==================== ======== ===========
:code:`message`           1      `Message`_           optional            
:code:`sender_name`       2      string               optional            
:code:`sender_avatar_url` 3      string               optional            
:code:`group_name`        4      string               optional            
:code:`cause`             5      `NotificationCause`_ optional            
========================= ====== ==================== ======== ===========

AndroidLocalNotification
------------------------

================== ====== ====== ======== =============================================================
Field              Number Type   Label    Description                                                  
================== ====== ====== ======== =============================================================
:code:`thread_id`  1      string optional                                                              
:code:`type_id`    2      string optional                                                              
:code:`group_name` 3      string optional                                                              
:code:`text_body`  6      string optional                                                              
:code:`icon_url`   4      string optional                                                              
:code:`title`      7      string optional optional ComGoogleProtobufAny chime_notification_payload = 5;
:code:`group_id`   8      string optional                                                              
:code:`sub_text`   9      string optional                                                              
================== ====== ====== ======== =============================================================

IosLocalNotification
--------------------

==================================== ====== ================================== ======== ===========
Field                                Number Type                               Label    Description
==================================== ====== ================================== ======== ===========
:code:`apns_collapse_id`             1      string                             optional            
:code:`thread_id`                    2      string                             optional            
:code:`title`                        3      string                             optional            
:code:`body`                         4      string                             optional            
:code:`category`                     5      string                             optional            
:code:`group_is_flat`                6      bool                               optional            
:code:`message_is_off_the_record`    7      bool                               optional            
:code:`attribute_checker_group_type` 9      `SharedAttributeCheckerGroupType`_ optional            
:code:`navigation`                   8      string                             optional            
==================================== ====== ================================== ======== ===========

MobileLocalNotification
-----------------------

================================== ====== =========================== ======== ===========
Field                              Number Type                        Label    Description
================================== ====== =========================== ======== ===========
:code:`android_local_notification` 1      `AndroidLocalNotification`_ optional            
:code:`ios_local_notification`     2      `IosLocalNotification`_     optional            
================================== ====== =========================== ======== ===========

MessageEvent
------------

================================== ====== ========== ======== ===========
Field                              Number Type       Label    Description
================================== ====== ========== ======== ===========
:code:`message`                    1      `Message`_ optional            
:code:`last_message_in_topic_time` 4      int64      optional            
:code:`prev_revision_time`         5      int64      optional            
:code:`is_head_message`            6      bool       optional            
================================== ====== ========== ======== ===========

TypingStateChangedEvent
-----------------------

============================ ====== ================ ======== ===========
Field                        Number Type             Label    Description
============================ ====== ================ ======== ===========
:code:`state`                1      `TypingState`_   optional            
:code:`user_id`              2      `UserId`_        optional            
:code:`context`              3      `TypingContext`_ optional            
:code:`start_timestamp_usec` 4      int64            optional            
============================ ====== ================ ======== ===========

MembershipChangedEvent
----------------------

============================== ====== ================== ======== ===========
Field                          Number Type               Label    Description
============================== ====== ================== ======== ===========
:code:`new_membership`         1      `Membership`_      optional            
:code:`prior_membership_state` 2      `MembershipState`_ optional            
:code:`prior_membership_role`  3      `MembershipRole`_  optional            
============================== ====== ================== ======== ===========

ReadReceiptChangedEvent
-----------------------

======================== ====== ================= ======== ===========
Field                    Number Type              Label    Description
======================== ====== ================= ======== ===========
:code:`group_id`         1      `GroupId`_        optional            
:code:`read_receipt_set` 2      `ReadReceiptSet`_ optional            
======================== ====== ================= ======== ===========

GroupViewedEvent
----------------

================= ====== ========== ======== ===========
Field             Number Type       Label    Description
================= ====== ========== ======== ===========
:code:`group_id`  1      `GroupId`_ optional            
:code:`view_time` 2      int64      optional            
================= ====== ========== ======== ===========

WebPushNotificationEvent
------------------------

================================= ====== ================================================ ======== ===========
Field                             Number Type                                             Label    Description
================================= ====== ================================================ ======== ===========
:code:`notification`              1      `WebPushNotification`_                           optional            
:code:`dispatch_approach_type`    2      `WebPushNotificationEvent.DispatchApproachType`_ optional            
:code:`mobile_local_notification` 3      `MobileLocalNotification`_                       optional            
:code:`endpoint_types`            4      `WebPushNotificationEvent.EndpointType`_         repeated            
================================= ====== ================================================ ======== ===========

WebPushNotificationEvent.DispatchApproachType
---------------------------------------------

========================================================== ====== ===========
Name                                                       Number Description
========================================================== ====== ===========
:code:`DISPATCH_APPROACH_TYPE_UNSPECIFIED`                 0                 
:code:`INTERACTIVE_SESSION`                                1                 
:code:`BROADCAST_TO_WEB_SESSIONS`                          2                 
:code:`INTERACTIVE_SESSIONS_INCLUDE_VISIBLE_WEB_UNDER_TTL` 3                 
========================================================== ====== ===========

WebPushNotificationEvent.EndpointType
-------------------------------------

================================= ====== ===========
Name                              Number Description
================================= ====== ===========
:code:`ENDPOINT_TYPE_UNSPECIFIED` 0                 
:code:`MOBILE_WEBCHANNEL`         1                 
:code:`DESKTOP_WEBCHANNEL`        2                 
================================= ====== ===========

StreamEventsRequest
-------------------

used in the /webchannel/events

================================ ====== ========================= ======== ===========
Field                            Number Type                      Label    Description
================================ ====== ========================= ======== ===========
:code:`platform`                 4      `Platform`_               optional            
:code:`client_info`              5      `ClientInfo`_             optional            
:code:`client_session_id`        6      int64                     optional            
:code:`sample_id`                1      string                    optional            
:code:`sample_ids`               7      string                    repeated            
:code:`ping_event`               2      `PingEvent`_              optional            
:code:`clock_sync_request`       3      `ClockSyncRequest`_       optional            
:code:`group_subscription_event` 8      `GroupSubscriptionEvent`_ optional            
:code:`test_user_gaia_id`        100    int64                     optional            
================================ ====== ========================= ======== ===========

ClientInfo
----------

====================== ====== ============== ======== ===========
Field                  Number Type           Label    Description
====================== ====== ============== ======== ===========
:code:`platform`       1      `Platform`_    optional            
:code:`origin`         2      `EventOrigin`_ optional            
:code:`source_machine` 3      string         optional            
====================== ====== ============== ======== ===========

ClientNotificationsState
------------------------

============================ ====== ========================================================== ======== ===========
Field                        Number Type                                                       Label    Description
============================ ====== ========================================================== ======== ===========
:code:`device_setting_state` 1      `ClientNotificationsState.DeviceNotificationSettingState`_ optional            
============================ ====== ========================================================== ======== ===========

ClientNotificationsState.DeviceNotificationSettingState
-------------------------------------------------------

================================================= ====== ===========
Name                                              Number Description
================================================= ====== ===========
:code:`DEVICE_NOTIFICATION_SETTING_STATE_UNKNOWN` 0                 
:code:`DEVICE_NOTIFICATIONS_ENABLED`              1                 
:code:`DEVICE_NOTIFICATIONS_DISABLED`             2                 
================================================= ====== ===========

PingEvent
---------

==================================== ====== =================================== ======== ===========
Field                                Number Type                                Label    Description
==================================== ====== =================================== ======== ===========
:code:`state`                        1      `PingEvent.State`_                  optional            
:code:`application_focus_state`      3      `PingEvent.ApplicationFocusState`_  optional            
:code:`last_interactive_time_ms`     4      int64                               optional            
:code:`client_interactive_state`     5      `PingEvent.ClientInteractiveState`_ optional            
:code:`client_notifications_enabled` 6      bool                                optional            
:code:`notifications_state`          7      `ClientNotificationsState`_         optional            
:code:`pwa_dedupe_enabled`           8      bool                                optional            
:code:`device_active_state`          9      `PingEvent.DeviceActiveState`_      optional            
==================================== ====== =================================== ======== ===========

PingEvent.State
---------------

========================= ====== ===========
Name                      Number Description
========================= ====== ===========
:code:`STATE_UNSPECIFIED` 0                 
:code:`ACTIVE`            1                 
:code:`INACTIVE`          2                 
========================= ====== ===========

PingEvent.ApplicationFocusState
-------------------------------

=============================== ====== ===========
Name                            Number Description
=============================== ====== ===========
:code:`FOCUS_STATE_UNSPECIFIED` 0                 
:code:`FOCUS_STATE_FOREGROUND`  1                 
:code:`FOCUS_STATE_BACKGROUND`  2                 
=============================== ====== ===========

PingEvent.ClientInteractiveState
--------------------------------

=================== ====== ===========
Name                Number Description
=================== ====== ===========
:code:`UNKNOWN`     0                 
:code:`INTERACTIVE` 1                 
:code:`FOCUSED`     2                 
:code:`VISIBLE`     3                 
:code:`HIDDEN`      4                 
=================== ====== ===========

PingEvent.DeviceActiveState
---------------------------

=========================== ====== ===========
Name                        Number Description
=========================== ====== ===========
:code:`STATE_UNKNOWN`       0                 
:code:`STATE_IDLE_LOCKED`   1                 
:code:`STATE_IDLE_UNLOCKED` 2                 
:code:`STATE_ACTIVE`        3                 
=========================== ====== ===========

ClockSyncRequest
----------------

======================== ====== ===== ======== ===========
Field                    Number Type  Label    Description
======================== ====== ===== ======== ===========
:code:`origin_time_msec` 1      int64 optional            
======================== ====== ===== ======== ===========

ClockSyncResponse
-----------------

========================== ====== ===== ======== ===========
Field                      Number Type  Label    Description
========================== ====== ===== ======== ===========
:code:`origin_time_msec`   1      int64 optional            
:code:`receive_time_msec`  2      int64 optional            
:code:`transmit_time_msec` 3      int64 optional            
========================== ====== ===== ======== ===========

GroupSubscriptionEvent
----------------------

================= ====== ========== ======== ===========
Field             Number Type       Label    Description
================= ====== ========== ======== ===========
:code:`group_ids` 1      `GroupId`_ repeated            
================= ====== ========== ======== ===========

StreamEventsResponse
--------------------

echo "<base64 encoded stream response>" | base64 -d - | protoc --decode StreamEventsResponse googlechat.proto

=========================== ====== ==================== ======== ===========
Field                       Number Type                 Label    Description
=========================== ====== ==================== ======== ===========
:code:`event`               1      `Event`_             optional            
:code:`sample_id`           2      string               optional            
:code:`clock_sync_response` 3      `ClockSyncResponse`_ optional            
=========================== ====== ==================== ======== ===========

Event
-----

====================== ====== ================== ======== ============================
Field                  Number Type               Label    Description                 
====================== ====== ================== ======== ============================
:code:`group_id`       1      `GroupId`_         optional                             
:code:`type`           3      `Event.EventType`_ optional                             
:code:`body`           4      `Event.EventBody`_ optional This is the first event body
:code:`user_id`        5      `UserId`_          optional                             
:code:`user_revision`  6      `WriteRevision`_   optional oneof RevisionType {        
:code:`group_revision` 7      `WriteRevision`_   optional                             
:code:`bodies`         8      `Event.EventBody`_ repeated }                           
====================== ====== ================== ======== ============================

Event.EventBody
---------------

================================== ====== =========================== ======== =================================================================================================================================================================================================================================================================================================================================================================================
Field                              Number Type                        Label    Description                                                                                                                                                                                                                                                                                                                                                                      
================================== ====== =========================== ======== =================================================================================================================================================================================================================================================================================================================================================================================
:code:`group_viewed`               3      `GroupViewedEvent`_         optional oneof Type {                                                                                                                                                                                                                                                                                                                                                                     
:code:`message_posted`             6      `MessageEvent`_             optional TopicViewedEvent topic_viewed = 4; GroupUpdatedEvent group_updated = 5;                                                                                                                                                                                                                                                                                                          
:code:`web_push_notification`      10     `WebPushNotificationEvent`_ optional TopicMuteChangedEvent topic_mute_changed = 7; UserSettingsChangedEvent user_settings_changed = 8; GroupStarredEvent group_starred = 9;                                                                                                                                                                                                                                           
:code:`membership_changed`         14     `MembershipChangedEvent`_   optional GroupUnreadSubscribedTopicCountUpdatedEvent group_unread_subscribed_topic_count_updated_event = 11; InviteCountUpdatedEvent invite_count_updated = 13;                                                                                                                                                                                                                           
:code:`user_status_updated_event`  23     `UserStatusUpdatedEvent`_   optional GroupHideChangedEvent group_hide_changed = 15; DriveAclFixProcessedEvent drive_acl_fix_processed = 16; GroupNotificationSettingsUpdatedEvent group_notification_settings_updated = 17; MessageDeletedEvent message_deleted = 18; RetentionSettingsUpdatedEvent retention_settings_updated = 19; TopicCreatedEvent topic_created = 21; MessageReactionEvent message_reaction = 22;
:code:`typing_state_changed_event` 26     `TypingStateChangedEvent`_  optional WorkingHoursSettingsUpdatedEvent working_hours_settings_updated_event = 24; MessageSmartRepliesEvent message_smart_replies_event = 25;                                                                                                                                                                                                                                           
:code:`read_receipt_changed`       33     `ReadReceiptChangedEvent`_  optional GroupDeletedEvent group_deleted_event = 27; BlockStateChangedEvent block_state_changed_event = 28; ClearHistoryEvent clear_history_event = 29; GroupSortTimestampChangedEvent group_sort_timestamp_changed_event = 30; MarkAsUnreadEvent mark_as_unread_event = 32;                                                                                                              
:code:`event_type`                 12     `Event.EventType`_          optional                                                                                                                                                                                                                                                                                                                                                                                  
:code:`trace_id`                   20     int64                       optional                                                                                                                                                                                                                                                                                                                                                                                  
================================== ====== =========================== ======== =================================================================================================================================================================================================================================================================================================================================================================================

Event.EventType
---------------

=================================================== ====== ===========
Name                                                Number Description
=================================================== ====== ===========
:code:`UNKNOWN`                                     0                 
:code:`USER_ADDED_TO_GROUP`                         1                 
:code:`USER_REMOVED_FROM_GROUP`                     2                 
:code:`GROUP_VIEWED`                                3                 
:code:`TOPIC_VIEWED`                                4                 
:code:`GROUP_UPDATED`                               5                 
:code:`MESSAGE_POSTED`                              6                 
:code:`MESSAGE_UPDATED`                             7                 
:code:`MESSAGE_DELETED`                             8                 
:code:`TOPIC_MUTE_CHANGED`                          9                 
:code:`USER_SETTINGS_CHANGED`                       10                
:code:`GROUP_STARRED`                               11                
:code:`WEB_PUSH_NOTIFICATION`                       12                
:code:`GROUP_UNREAD_SUBSCRIBED_TOPIC_COUNT_UPDATED` 13                
:code:`INVITE_COUNT_UPDATED`                        14                
:code:`MEMBERSHIP_CHANGED`                          15                
:code:`GROUP_HIDE_CHANGED`                          16                
:code:`DRIVE_ACL_FIX_PROCESSED`                     17                
:code:`GROUP_NOTIFICATION_SETTINGS_UPDATED`         18                
:code:`RETENTION_SETTINGS_UPDATED`                  19                
:code:`TOPIC_CREATED`                               20                
:code:`ON_HOLD_MESSAGE_POSTED`                      21                
:code:`ON_HOLD_MESSAGE_UPDATED`                     22                
:code:`ON_HOLD_MESSAGE_PUBLISHED`                   23                
:code:`MESSAGE_REACTED`                             24                
:code:`USER_STATUS_UPDATED_EVENT`                   25                
:code:`GROUP_RETENTION_SETTINGS_UPDATED`            26                
:code:`USER_WORKING_HOURS_UPDATED_EVENT`            27                
:code:`MESSAGE_SMART_REPLIES`                       28                
:code:`TYPING_STATE_CHANGED`                        29                
:code:`GROUP_DELETED`                               30                
:code:`BLOCK_STATE_CHANGED`                         31                
:code:`CLEAR_HISTORY`                               32                
:code:`SESSION_READY`                               33                
:code:`GROUP_SORT_TIMESTAMP_CHANGED`                34                
:code:`GSUITE_INTEGRATION_UPDATED`                  35                
:code:`READ_RECEIPT_CHANGED`                        36                
:code:`MARK_AS_UNREAD`                              37                
:code:`GROUP_NO_OP`                                 38                
:code:`INVALIDATE_GROUP_CACHE`                      39                
:code:`USER_NO_OP`                                  40                
:code:`INVALIDATE_USER_CACHE`                       41                
:code:`USER_DENORMALIZED_GROUP_UPDATED`             42                
:code:`USER_PRESENCE_SHARED_UPDATED_EVENT`          43                
:code:`NOTIFICATIONS_CARD_UPDATED`                  44                
:code:`USER_HUB_AVAILABILITY_UPDATED_EVENT`         45                
:code:`USER_OWNERSHIP_UPDATED`                      46                
:code:`SHARED_DRIVE_CREATE_SCHEDULED`               47                
:code:`SHARED_DRIVE_UPDATED`                        48                
:code:`MESSAGE_PERSONAL_LABEL_UPDATED`              49                
:code:`USER_QUOTA_EXCEEDED`                         50                
=================================================== ====== ===========

InviteType
----------

===== ====== ==== ===== ===========
Field Number Type Label Description
===== ====== ==== ===== ===========
===== ====== ==== ===== ===========

InviteType.Type
---------------

================== ====== ===========
Name               Number Description
================== ====== ===========
:code:`UNKNOWN`    0                 
:code:`AT_MENTION` 1                 
:code:`DIRECT_ADD` 2                 
================== ====== ===========

GroupDetails
------------

=================== ====== ====== ======== ===========
Field               Number Type   Label    Description
=================== ====== ====== ======== ===========
:code:`description` 1      string optional            
:code:`guidelines`  2      string optional            
=================== ====== ====== ======== ===========

RoomUpdatedMetadata
-------------------

================================== ====== ================================================== ======== ===========
Field                              Number Type                                               Label    Description
================================== ====== ================================================== ======== ===========
:code:`name`                       2      string                                             optional            
:code:`visibility`                 3      `GroupVisibility`_                                 optional            
:code:`group_link_sharing_enabled` 5      bool                                               optional            
:code:`rename_metadata`            6      `RoomUpdatedMetadata.RoomRenameMetadata`_          optional            
:code:`group_details_metadata`     7      `RoomUpdatedMetadata.GroupDetailsUpdatedMetadata`_ optional            
:code:`initiator`                  4      `User`_                                            optional            
================================== ====== ================================================== ======== ===========

RoomUpdatedMetadata.RoomRenameMetadata
--------------------------------------

================= ====== ====== ======== ===========
Field             Number Type   Label    Description
================= ====== ====== ======== ===========
:code:`new_name`  1      string optional            
:code:`prev_name` 2      string optional            
================= ====== ====== ======== ===========

RoomUpdatedMetadata.GroupDetailsUpdatedMetadata
-----------------------------------------------

========================== ====== =============== ======== ===========
Field                      Number Type            Label    Description
========================== ====== =============== ======== ===========
:code:`new_group_details`  1      `GroupDetails`_ optional            
:code:`prev_group_details` 2      `GroupDetails`_ optional            
========================== ====== =============== ======== ===========

MeetingSpace
------------

ComGoogleRtcMeetingsV1MeetingSpace

============================== ====== ==================================== ======== ===========
Field                          Number Type                                 Label    Description
============================== ====== ==================================== ======== ===========
:code:`meeting_space_id`       1      string                               optional            
:code:`meeting_code`           2      string                               optional            
:code:`meeting_url`            3      string                               optional            
:code:`phone_access`           5      `MeetingSpace.PhoneAccess`_          repeated            
:code:`universal_phone_access` 8      `MeetingSpace.UniversalPhoneAccess`_ optional            
:code:`call_info`              6      `MeetingSpace.CallInfo`_             optional            
:code:`meeting_alias`          7      string                               optional            
:code:`gateway_access`         9      `MeetingSpace.GatewayAccess`_        optional            
:code:`more_join_url`          10     string                               optional            
:code:`accepted_number_class`  11     `DialInNumberClass`_                 repeated            
:code:`gateway_sip_access`     13     `MeetingSpace.GatewaySipAccess`_     repeated            
:code:`broadcast_access`       14     `MeetingSpace.BroadcastAccess`_      optional            
:code:`settings`               15     `MeetingSpace.Settings`_             optional            
============================== ====== ==================================== ======== ===========

MeetingSpace.PhoneAccess
------------------------

============================== ====== ====== ======== ===========
Field                          Number Type   Label    Description
============================== ====== ====== ======== ===========
:code:`phone_number`           1      string optional            
:code:`formatted_phone_number` 5      string optional            
:code:`pin`                    2      string optional            
:code:`region_code`            3      string optional            
:code:`language_code`          4      string optional            
============================== ====== ====== ======== ===========

MeetingSpace.UniversalPhoneAccess
---------------------------------

===================== ====== ====== ======== ===========
Field                 Number Type   Label    Description
===================== ====== ====== ======== ===========
:code:`pin`           1      string optional            
:code:`pstn_info_url` 2      string optional            
===================== ====== ====== ======== ===========

MeetingSpace.Presenter
----------------------

=========================== ====== ====== ======== ===========
Field                       Number Type   Label    Description
=========================== ====== ====== ======== ===========
:code:`presenter_device_id` 1      string optional            
:code:`by_device_id`        2      string optional            
=========================== ====== ====== ======== ===========

MeetingSpace.RecordingInfo
--------------------------

================================== ====== ============================================= ======== ===========
Field                              Number Type                                          Label    Description
================================== ====== ============================================= ======== ===========
:code:`recording_status`           1      `MeetingSpace.RecordingInfo.RecordingStatus`_ optional            
:code:`recording_id`               2      string                                        optional            
:code:`producer_device_id`         3      string                                        optional            
:code:`latest_recording_event`     4      `MeetingSpace.RecordingInfo.RecordingEvent`_  optional            
:code:`owner_display_name`         5      string                                        optional            
:code:`recording_application_type` 6      `RecordingApplicationType`_                   optional            
================================== ====== ============================================= ======== ===========

MeetingSpace.RecordingInfo.RecordingEvent
-----------------------------------------

================= ====== ====================================================== ======== ===========
Field             Number Type                                                   Label    Description
================= ====== ====================================================== ======== ===========
:code:`device_id` 1      string                                                 optional            
:code:`type`      2      `MeetingSpace.RecordingInfo.RecordingEvent.EventType`_ optional            
================= ====== ====================================================== ======== ===========

MeetingSpace.RecordingInfo.RecordingEvent.EventType
---------------------------------------------------

=================================== ====== ===========
Name                                Number Description
=================================== ====== ===========
:code:`RECORDING_EVENT_UNSPECIFIED` 0                 
:code:`USER_ACTION`                 1                 
=================================== ====== ===========

MeetingSpace.RecordingInfo.RecordingStatus
------------------------------------------

============================= ====== ===========
Name                          Number Description
============================= ====== ===========
:code:`RECORDING_UNSPECIFIED` 0                 
:code:`RECORDING_INACTIVE`    1                 
:code:`RECORDING_STARTING`    2                 
:code:`RECORDING_STARTED`     3                 
============================= ====== ===========

MeetingSpace.StreamingSessionInfo
---------------------------------

============================ ====== ====================================================== ======== ===========
Field                        Number Type                                                   Label    Description
============================ ====== ====================================================== ======== ===========
:code:`status`               1      `MeetingSpace.StreamingSessionInfo.Status`_            optional            
:code:`session_id`           2      string                                                 optional            
:code:`application_type`     3      `RecordingApplicationType`_                            optional            
:code:`latest_session_event` 4      `MeetingSpace.StreamingSessionInfo.SessionEvent`_      optional            
:code:`owner_display_name`   5      string                                                 optional            
:code:`viewer_access_policy` 6      `BroadcastAccessPolicy`_                               optional            
:code:`training_enabled`     7      bool                                                   optional            
:code:`viewer_stats`         8      `MeetingSpace.StreamingSessionInfo.StreamViewerStats`_ optional            
============================ ====== ====================================================== ======== ===========

MeetingSpace.StreamingSessionInfo.SessionEvent
----------------------------------------------

================= ====== =========================================================== ======== ===========
Field             Number Type                                                        Label    Description
================= ====== =========================================================== ======== ===========
:code:`device_id` 1      string                                                      optional            
:code:`type`      2      `MeetingSpace.StreamingSessionInfo.SessionEvent.EventType`_ optional            
================= ====== =========================================================== ======== ===========

MeetingSpace.StreamingSessionInfo.SessionEvent.EventType
--------------------------------------------------------

========================= ====== ===========
Name                      Number Description
========================= ====== ===========
:code:`EVENT_UNSPECIFIED` 0                 
========================= ====== ===========

MeetingSpace.StreamingSessionInfo.StreamViewerStats
---------------------------------------------------

============================== ====== ===== ======== ===========
Field                          Number Type  Label    Description
============================== ====== ===== ======== ===========
:code:`estimated_viewer_count` 1      int64 optional            
============================== ====== ===== ======== ===========

MeetingSpace.StreamingSessionInfo.Status
----------------------------------------

========================== ====== ===========
Name                       Number Description
========================== ====== ===========
:code:`STATUS_UNSPECIFIED` 0                 
:code:`INACTIVE`           1                 
:code:`STARTING`           2                 
:code:`LIVE`               3                 
========================== ====== ===========

MeetingSpace.CallInfo
---------------------

=================================== ====== ===================================== ======== ===========
Field                               Number Type                                  Label    Description
=================================== ====== ===================================== ======== ===========
:code:`presenter`                   1      `MeetingSpace.Presenter`_             optional            
:code:`recording_info`              2      `MeetingSpace.RecordingInfo`_         optional            
:code:`calendar_event_id`           3      string                                optional            
:code:`organization_name`           4      string                                optional            
:code:`max_joined_devices`          6      int32                                 optional            
:code:`media_backend_info`          8      string                                optional            
:code:`streaming_sessions`          9      `MeetingSpace.StreamingSessionInfo`_  repeated            
:code:`settings`                    11     `MeetingSpace.CallInfo.CallSettings`_ optional            
:code:`paygate_info`                13     `MeetingSpace.CallInfo.PaygateInfo`_  optional            
:code:`supported_caption_languages` 14     string                                repeated            
:code:`cse_info`                    15     `MeetingSpace.CallInfo.CseInfo`_      optional            
=================================== ====== ===================================== ======== ===========

MeetingSpace.CallInfo.CallSettings
----------------------------------

================================= ====== ==== ======== ===========
Field                             Number Type Label    Description
================================= ====== ==== ======== ===========
:code:`access_lock`               1      bool optional            
:code:`chat_lock`                 2      bool optional            
:code:`present_lock`              3      bool optional            
:code:`attendance_report_enabled` 4      bool optional            
:code:`audio_lock`                5      bool optional            
:code:`video_lock`                6      bool optional            
:code:`moderation_enabled`        7      bool optional            
:code:`cse_enabled`               8      bool optional            
================================= ====== ==== ======== ===========

MeetingSpace.CallInfo.PaygateInfo
---------------------------------

repeated ComGoogleRtcMeetingsV1DocumentInfo attached_documents = 12;

===================================== ====== ============================= ======== ===========
Field                                 Number Type                          Label    Description
===================================== ====== ============================= ======== ===========
:code:`show_upgrade_promos`           1      bool                          optional            
:code:`call_ending_soon_warning_time` 2      `ComGoogleProtobufTimestamp`_ optional            
:code:`call_ending_time`              3      `ComGoogleProtobufTimestamp`_ optional            
===================================== ====== ============================= ======== ===========

MeetingSpace.CallInfo.CseInfo
-----------------------------

=================== ====== ===== ======== ===========
Field               Number Type  Label    Description
=================== ====== ===== ======== ===========
:code:`wrapped_key` 1      bytes optional            
=================== ====== ===== ======== ===========

MeetingSpace.GatewayAccess
--------------------------

=============== ====== ==== ======== ===========
Field           Number Type Label    Description
=============== ====== ==== ======== ===========
:code:`enabled` 1      bool optional            
=============== ====== ==== ======== ===========

MeetingSpace.GatewaySipAccess
-----------------------------

======================= ====== ====== ======== ===========
Field                   Number Type   Label    Description
======================= ====== ====== ======== ===========
:code:`uri`             1      string optional            
:code:`sip_access_code` 2      string optional            
======================= ====== ====== ======== ===========

MeetingSpace.BroadcastAccess
----------------------------

================ ====== ====== ======== =======================================================================
Field            Number Type   Label    Description                                                            
================ ====== ====== ======== =======================================================================
:code:`view_url` 1      string optional optional ComGoogleRtcMeetingsV1BroadcastAccessPolicy access_policy = 2;
================ ====== ====== ======== =======================================================================

MeetingSpace.Settings
---------------------

================================= ====== ==== ======== ===========
Field                             Number Type Label    Description
================================= ====== ==== ======== ===========
:code:`access_lock`               1      bool optional            
:code:`attendance_report_enabled` 2      bool optional            
:code:`chat_lock`                 3      bool optional            
:code:`present_lock`              4      bool optional            
:code:`moderation_enabled`        5      bool optional            
:code:`cse_enabled`               6      bool optional            
================================= ====== ==== ======== ===========

VideoCallMetadata
-----------------

==================================== ====== =============== ======== ===========
Field                                Number Type            Label    Description
==================================== ====== =============== ======== ===========
:code:`meeting_space`                1      `MeetingSpace`_ optional            
:code:`was_created_in_current_group` 2      bool            optional            
:code:`should_not_render`            3      bool            optional            
==================================== ====== =============== ======== ===========

MembershipChangedMetadata
-------------------------

================================ ====== =============================================== ======== ===========
Field                            Number Type                                            Label    Description
================================ ====== =============================================== ======== ===========
:code:`type`                     1      `MembershipChangedMetadata.Type`_               optional            
:code:`affected_memberships`     6      `MembershipChangedMetadata.AffectedMembership`_ repeated            
:code:`initiator`                2      `UserId`_                                       optional            
:code:`affected_members`         3      `MemberId`_                                     repeated            
:code:`initiator_profile`        4      `User`_                                         optional            
:code:`affected_member_profiles` 5      `Member`_                                       repeated            
================================ ====== =============================================== ======== ===========

MembershipChangedMetadata.AffectedMembership
--------------------------------------------

============================== ====== ================== ======== ===========
Field                          Number Type               Label    Description
============================== ====== ================== ======== ===========
:code:`affected_member`        1      `MemberId`_        optional            
:code:`prior_membership_state` 2      `MembershipState`_ optional            
:code:`prior_membership_role`  3      `MembershipRole`_  optional            
:code:`target_membership_role` 4      `MembershipRole`_  optional            
============================== ====== ================== ======== ===========

MembershipChangedMetadata.Type
------------------------------

================================== ====== ===========
Name                               Number Description
================================== ====== ===========
:code:`TYPE_UNSPECIFIED`           0                 
:code:`INVITED`                    1                 
:code:`JOINED`                     2                 
:code:`ADDED`                      3                 
:code:`REMOVED`                    4                 
:code:`LEFT`                       5                 
:code:`BOT_ADDED`                  6                 
:code:`BOT_REMOVED`                7                 
:code:`KICKED_DUE_TO_OTR_CONFLICT` 8                 
:code:`ROLE_UPDATED`               9                 
================================== ====== ===========

UserMentionMetadata
-------------------

==================== ====== =========================== ======== ===========
Field                Number Type                        Label    Description
==================== ====== =========================== ======== ===========
:code:`id`           1      `UserId`_                   optional            
:code:`invitee_info` 3      `InviteeInfo`_              optional            
:code:`type`         2      `UserMentionMetadata.Type`_ optional            
:code:`display_name` 4      string                      optional            
:code:`gender`       5      string                      optional            
==================== ====== =========================== ======== ===========

UserMentionMetadata.Type
------------------------

======================== ====== ===========
Name                     Number Description
======================== ====== ===========
:code:`TYPE_UNSPECIFIED` 0                 
:code:`INVITE`           1                 
:code:`UNINVITE`         2                 
:code:`MENTION`          3                 
:code:`MENTION_ALL`      4                 
:code:`FAILED_TO_ADD`    5                 
======================== ====== ===========

SlashCommandMetadata
--------------------

======================= ====== ============================ ======== ===========
Field                   Number Type                         Label    Description
======================= ====== ============================ ======== ===========
:code:`id`              1      `UserId`_                    optional            
:code:`type`            2      `SlashCommandMetadata.Type`_ optional            
:code:`command_name`    3      string                       optional            
:code:`command_id`      4      int64                        optional            
:code:`arguments_hint`  5      string                       optional            
:code:`triggers_dialog` 6      bool                         optional            
======================= ====== ============================ ======== ===========

SlashCommandMetadata.Type
-------------------------

======================== ====== ===========
Name                     Number Description
======================== ====== ===========
:code:`TYPE_UNSPECIFIED` 0                 
:code:`ADD`              1                 
:code:`INVOKE`           2                 
:code:`FAILED_TO_ADD`    3                 
======================== ====== ===========

GroupVisibility
---------------

===== ====== ==== ===== ===========
Field Number Type Label Description
===== ====== ==== ===== ===========
===== ====== ==== ===== ===========

GroupVisibility.VisibilityState
-------------------------------

=============== ====== ===========
Name            Number Description
=============== ====== ===========
:code:`UNKNOWN` 0                 
:code:`PRIVATE` 1                 
:code:`PUBLIC`  2                 
=============== ====== ===========

ComGoogleProtobufTimestamp
--------------------------

=============== ====== ===== ======== ===========
Field           Number Type  Label    Description
=============== ====== ===== ======== ===========
:code:`seconds` 1      int64 optional            
:code:`nanos`   2      int32 optional            
=============== ====== ===== ======== ===========

GetServerTimeRequest
--------------------

====================== ====== ================ ======== ===========
Field                  Number Type             Label    Description
====================== ====== ================ ======== ===========
:code:`request_header` 100    `RequestHeader`_ optional            
====================== ====== ================ ======== ===========

GetServerTimeResponse
---------------------

================= ====== ============================= ======== ===========
Field             Number Type                          Label    Description
================= ====== ============================= ======== ===========
:code:`timestamp` 1      `ComGoogleProtobufTimestamp`_ optional            
================= ====== ============================= ======== ===========

CatchUpRange
------------

=============================== ====== ===== ======== ===========
Field                           Number Type  Label    Description
=============================== ====== ===== ======== ===========
:code:`from_revision_timestamp` 1      int64 optional            
:code:`to_revision_timestamp`   2      int64 optional            
=============================== ====== ===== ======== ===========

CatchUpGroupRequest
-------------------

====================== ====== ================ ======== ===========
Field                  Number Type             Label    Description
====================== ====== ================ ======== ===========
:code:`request_header` 100    `RequestHeader`_ optional            
:code:`group_id`       1      `GroupId`_       optional            
:code:`range`          2      `CatchUpRange`_  optional            
:code:`page_size`      3      int32            optional            
:code:`cutoff_size`    4      int32            optional            
====================== ====== ================ ======== ===========

CatchUpUserRequest
------------------

====================== ====== ================ ======== ===========
Field                  Number Type             Label    Description
====================== ====== ================ ======== ===========
:code:`request_header` 100    `RequestHeader`_ optional            
:code:`range`          1      `CatchUpRange`_  optional            
:code:`page_size`      2      int32            optional            
:code:`cutoff_size`    3      int32            optional            
====================== ====== ================ ======== ===========

CatchUpResponse
---------------

Used by both CatchUpGroupRequest and CatchUpUserRequest

================== ====== ================================= ======== ===========
Field              Number Type                              Label    Description
================== ====== ================================= ======== ===========
:code:`events`     1      `Event`_                          repeated            
:code:`status`     2      `CatchUpResponse.ResponseStatus`_ optional            
:code:`group_data` 3      `GroupData`_                      optional            
================== ====== ================================= ======== ===========

CatchUpResponse.ResponseStatus
------------------------------

===================================== ====== ===========
Name                                  Number Description
===================================== ====== ===========
:code:`UNKNOWN`                       0                 
:code:`COMPLETED`                     1                 
:code:`PAGINATED`                     2                 
:code:`ABORTED_CUTOFF_EXCEEDED`       3                 
:code:`ABORTED_CACHE_INVALIDATION`    4                 
:code:`ABORTED_FROM_REVISION_TOO_OLD` 5                 
===================================== ====== ===========

GetGroupRequest
---------------

============================ ====== =============================== ======== ===========
Field                        Number Type                            Label    Description
============================ ====== =============================== ======== ===========
:code:`request_header`       100    `RequestHeader`_                optional            
:code:`group_id`             1      `GroupId`_                      optional            
:code:`fetch_options`        4      `GetGroupRequest.FetchOptions`_ repeated            
:code:`user_not_older_than`  2      `ReferenceRevision`_            optional            
:code:`group_not_older_than` 3      `ReferenceRevision`_            optional            
:code:`include_invite_dms`   5      bool                            optional            
============================ ====== =============================== ======== ===========

GetGroupRequest.FetchOptions
----------------------------

========================================= ====== ===========
Name                                      Number Description
========================================= ====== ===========
:code:`MEMBERS`                           0                 
:code:`INVITEES`                          1                 
:code:`MEMBER_IDS_ONLY`                   2                 
:code:`PROFILE_IN_READ_RECEIPTS`          3                 
:code:`INCLUDE_SNIPPET`                   4                 
:code:`INCLUDE_DYNAMIC_GROUP_NAME`        5                 
:code:`INCLUDE_ACCOUNT_USER_CAPABILITIES` 6                 
========================================= ====== ===========

GetGroupResponse
----------------

========================== ====== ================== ======== ==============================
Field                      Number Type               Label    Description                   
========================== ====== ================== ======== ==============================
:code:`group`              1      `Group`_           optional                               
:code:`memberships`        4      `Membership`_      repeated                               
:code:`user_revision`      2      `ReadRevision`_    optional                               
:code:`group_revision`     3      `ReadRevision`_    optional                               
:code:`membership_state`   6      `MembershipState`_ optional                               
:code:`joined_member_ids`  7      `MemberId`_        repeated                               
:code:`invited_member_ids` 8      `MemberId`_        repeated                               
:code:`read_receipt_set`   9      `ReadReceiptSet`_  optional                               
:code:`snippet`            10     `Message`_         optional repeated ?? capabilities = 11;
========================== ====== ================== ======== ==============================

WorldSection
------------

========================== ====== ================================ ======== ===========
Field                      Number Type                             Label    Description
========================== ====== ================================ ======== ===========
:code:`world_section_type` 1      `WorldSection.WorldSectionType`_ optional            
========================== ====== ================================ ======== ===========

WorldSection.WorldSectionType
-----------------------------

===================================================== ====== ===========
Name                                                  Number Description
===================================================== ====== ===========
:code:`WORLD_SECTION_TYPE_UNSPECIFIED`                0                 
:code:`STARRED_DIRECT_MESSAGE_PEOPLE`                 1                 
:code:`STARRED_ROOMS`                                 2                 
:code:`STARRED_DIRECT_MESSAGE_BOTS`                   3                 
:code:`NON_STARRED_DIRECT_MESSAGE_PEOPLE`             4                 
:code:`NON_STARRED_ROOMS`                             5                 
:code:`NON_STARRED_DIRECT_MESSAGE_BOTS`               6                 
:code:`ALL_DIRECT_MESSAGE_PEOPLE`                     7                 
:code:`ALL_ROOMS`                                     8                 
:code:`ALL_DIRECT_MESSAGE_BOTS`                       9                 
:code:`INVITED_DM_PEOPLE`                             10                
:code:`SPAM_INVITED_DM_PEOPLE`                        11                
:code:`STARRED_DIRECT_MESSAGE_EVERYONE`               12                
:code:`NON_STARRED_DIRECT_MESSAGE_EVERYONE`           13                
:code:`ALL_DIRECT_MESSAGE_EVERYONE`                   14                
:code:`STARRED_DMS_AND_STARRED_UNNAMED_ROOMS`         15                
:code:`NON_STARRED_DMS_AND_NON_STARRED_UNNAMED_ROOMS` 16                
===================================================== ====== ===========

WorldFilter
-----------

======================== ====== ============================== ======== ===========
Field                    Number Type                           Label    Description
======================== ====== ============================== ======== ===========
:code:`starred_state`    1      `WorldFilter.StarredState`_    optional            
:code:`visibility_state` 2      `WorldFilter.VisibilityState`_ optional            
:code:`read_state`       7      `WorldFilter.ReadState`_       optional            
:code:`block_state`      8      `WorldFilter.BlockState`_      optional            
:code:`named_state`      9      `WorldFilter.NamedState`_      optional            
:code:`membership_state` 3      `MembershipState`_             optional            
:code:`invite_category`  4      `InviteCategory`_              optional            
:code:`member_type`      5      `WorldFilter.MemberType`_      optional            
:code:`group_type`       6      `WorldFilter.GroupType`_       optional            
======================== ====== ============================== ======== ===========

WorldFilter.StarredState
------------------------

================================= ====== ===========
Name                              Number Description
================================= ====== ===========
:code:`STARRED_STATE_UNSPECIFIED` 0                 
:code:`STARRED`                   1                 
:code:`NON_STARRED`               2                 
================================= ====== ===========

WorldFilter.VisibilityState
---------------------------

==================================== ====== ===========
Name                                 Number Description
==================================== ====== ===========
:code:`VISIBILITY_STATE_UNSPECIFIED` 0                 
:code:`VISIBLE`                      1                 
:code:`HIDDEN`                       2                 
==================================== ====== ===========

WorldFilter.ReadState
---------------------

=============================== ====== ===========
Name                            Number Description
=============================== ====== ===========
:code:`READ_STATE_UNSPECIFIED`  0                 
:code:`READ`                    1                 
:code:`UNREAD`                  2                 
:code:`UNREAD_SUBSCRIBED_TOPIC` 3                 
=============================== ====== ===========

WorldFilter.BlockState
----------------------

=============================== ====== ===========
Name                            Number Description
=============================== ====== ===========
:code:`BLOCK_STATE_UNSPECIFIED` 0                 
:code:`BLOCKED`                 1                 
:code:`UNBLOCKED`               2                 
=============================== ====== ===========

WorldFilter.NamedState
----------------------

=============================== ====== ===========
Name                            Number Description
=============================== ====== ===========
:code:`NAMED_STATE_UNSPECIFIED` 0                 
:code:`NAMED`                   1                 
:code:`UNNAMED`                 2                 
=============================== ====== ===========

WorldFilter.MemberType
----------------------

=============================== ====== ===========
Name                            Number Description
=============================== ====== ===========
:code:`MEMBER_TYPE_UNSPECIFIED` 0                 
:code:`HUMAN`                   1                 
:code:`BOT`                     2                 
=============================== ====== ===========

WorldFilter.GroupType
---------------------

============================== ====== ===========
Name                           Number Description
============================== ====== ===========
:code:`GROUP_TYPE_UNSPECIFIED` 0                 
:code:`DM`                     1                 
:code:`ROOM`                   2                 
============================== ====== ===========

NameUsers
---------

=========================== ====== ========= ======== ===========
Field                       Number Type      Label    Description
=========================== ====== ========= ======== ===========
:code:`name_user_ids`       1      `UserId`_ repeated            
:code:`has_more_name_users` 2      bool      optional            
:code:`group_name`          3      string    optional            
=========================== ====== ========= ======== ===========

WorldItemLite
-------------

===================================== ====== ================================== ======== =========================================================================================================================================================================
Field                                 Number Type                               Label    Description                                                                                                                                                              
===================================== ====== ================================== ======== =========================================================================================================================================================================
:code:`group_id`                      1      `GroupId`_                         optional                                                                                                                                                                          
:code:`group_revision`                2      `ReadRevision`_                    optional                                                                                                                                                                          
:code:`sort_timestamp`                3      int64                              optional                                                                                                                                                                          
:code:`read_state`                    4      `GroupReadState`_                  optional                                                                                                                                                                          
:code:`room_name`                     5      string                             optional                                                                                                                                                                          
:code:`dm_members`                    6      `WorldItemLite.DmMembers`_         optional                                                                                                                                                                          
:code:`name_users`                    20     `NameUsers`_                       optional                                                                                                                                                                          
:code:`retention_horizon_time_micros` 8      int64                              optional                                                                                                                                                                          
:code:`retention_duration_seconds`    9      int64                              optional                                                                                                                                                                          
:code:`group_lite`                    7      `WorldItemLite.GroupLite`_         optional                                                                                                                                                                          
:code:`message`                       13     `Message`_                         optional optional GroupGuestAccessSettings group_guest_access_settings = 10; optional OrganizationInfo organization_info = 11; repeated GsuiteIntegration gsuite_integration = 12;
:code:`is_message_blocked`            25     bool                               optional                                                                                                                                                                          
:code:`flat_group`                    14     `WorldItemLite.FlatGroup`_         optional                                                                                                                                                                          
:code:`threaded_group`                15     `WorldItemLite.ThreadedGroup`_     optional                                                                                                                                                                          
:code:`avatar_url`                    16     string                             optional                                                                                                                                                                          
:code:`attribute_checker_group_type`  19     `SharedAttributeCheckerGroupType`_ optional repeated ?? space_integration_payloads = 17; optional GroupIntegrationSettings group_integration_settings = 22;  optional AvatarInfo avatar_info = 18;                   
:code:`group_support_level`           23     `GroupSupportLevel`_               optional                                                                                                                                                                          
:code:`group_unsupported_reason`      24     `GroupUnsupportedReason`_          optional optional ?? group_policies = 26;                                                                                                                                         
===================================== ====== ================================== ======== =========================================================================================================================================================================

WorldItemLite.MembershipLite
----------------------------

======================== ====== ================== ======== ===========
Field                    Number Type               Label    Description
======================== ====== ================== ======== ===========
:code:`user_id`          1      `UserId`_          optional            
:code:`membership_state` 2      `MembershipState`_ optional            
======================== ====== ================== ======== ===========

WorldItemLite.DmMembers
-----------------------

=========================== ====== =============================== ======== ===========
Field                       Number Type                            Label    Description
=========================== ====== =============================== ======== ===========
:code:`members`             1      `UserId`_                       repeated            
:code:`memberships`         2      `WorldItemLite.MembershipLite`_ repeated            
:code:`has_all_memberships` 3      bool                            optional            
=========================== ====== =============================== ======== ===========

WorldItemLite.GroupLite
-----------------------

========================== ====== ==================== ======== =========================================================
Field                      Number Type                 Label    Description                                              
========================== ====== ==================== ======== =========================================================
:code:`creator_id`         1      `UserId`_            optional                                                          
:code:`create_time`        2      int64                optional                                                          
:code:`is_flat`            3      bool                 optional                                                          
:code:`retention_settings` 4      `RetentionSettings`_ optional                                                          
:code:`interop_enabled`    5      bool                 optional                                                          
:code:`roster_email`       7      string               optional optional DasherDomainPolicies dasher_domain_policies = 6;
========================== ====== ==================== ======== =========================================================

WorldItemLite.FlatGroup
-----------------------

===== ====== ==== ===== ===========
Field Number Type Label Description
===== ====== ==== ===== ===========
===== ====== ==== ===== ===========

WorldItemLite.ThreadedGroup
---------------------------

===== ====== ==== ===== ===========
Field Number Type Label Description
===== ====== ==== ===== ===========
===== ====== ==== ===== ===========

WorldSectionRequest
-------------------

==================================== ====== =============== ======== ==================
Field                                Number Type            Label    Description       
==================================== ====== =============== ======== ==================
:code:`page_size`                    1      int32           optional                   
:code:`world_section`                2      `WorldSection`_ optional                   
:code:`world_filter`                 4      `WorldFilter`_  optional                   
:code:`num_world_items_with_snippet` 5      int32           optional                   
:code:`anchor_sort_timestamp_micros` 3      int64           optional                   
:code:`pagination_token`             6      string          optional oneof Pagination {
==================================== ====== =============== ======== ==================

WorldSectionResponse
--------------------

============================================== ====== ================ ======== ===========
Field                                          Number Type             Label    Description
============================================== ====== ================ ======== ===========
:code:`world_section`                          1      `WorldSection`_  optional            
:code:`world_filter`                           4      `WorldFilter`_   optional            
:code:`world_items`                            2      `WorldItemLite`_ repeated            
:code:`next_page_anchor_sort_timestamp_micros` 3      int64            optional            
:code:`has_more_items`                         5      bool             optional            
:code:`pagination_token`                       6      string           optional            
============================================== ====== ================ ======== ===========

PaginatedWorldRequest
---------------------

========================================== ====== ===================================== ======== ===========
Field                                      Number Type                                  Label    Description
========================================== ====== ===================================== ======== ===========
:code:`request_header`                     1      `RequestHeader`_                      optional            
:code:`world_section_requests`             2      `WorldSectionRequest`_                repeated            
:code:`world_consistency_token`            3      string                                optional            
:code:`fetch_options`                      4      `PaginatedWorldRequest.FetchOptions`_ repeated            
:code:`fetch_from_user_spaces`             5      bool                                  optional            
:code:`receive_world_update_notifications` 6      bool                                  optional            
:code:`fetch_snippets_for_unnamed_rooms`   7      bool                                  optional            
========================================== ====== ===================================== ======== ===========

PaginatedWorldRequest.FetchOptions
----------------------------------

======================================== ====== ===========
Name                                     Number Description
======================================== ====== ===========
:code:`UNKNOWN`                          0                 
:code:`EXCLUDE_GROUP_LITE`               1                 
:code:`FETCH_BOTS_IN_HUMAN_DM`           2                 
:code:`FETCH_SPACE_INTEGRATION_PAYLOADS` 3                 
:code:`FETCH_GROUPS_D3_POLICIES`         4                 
======================================== ====== ===========

PaginatedWorldResponse
----------------------

=============================== ====== ======================= ======== ===========
Field                           Number Type                    Label    Description
=============================== ====== ======================= ======== ===========
:code:`world_section_responses` 1      `WorldSectionResponse`_ repeated            
:code:`world_consistency_token` 2      string                  optional            
:code:`user_revision`           3      `ReadRevision`_         optional            
:code:`world_items`             4      `WorldItemLite`_        repeated            
=============================== ====== ======================= ======== ===========

RemoveMembershipsRequest
------------------------

======================== ====== ================== ======== ===========
Field                    Number Type               Label    Description
======================== ====== ================== ======== ===========
:code:`request_header`   100    `RequestHeader`_   optional            
:code:`member_ids`       1      `MemberId`_        repeated            
:code:`group_id`         2      `GroupId`_         optional            
:code:`membership_state` 3      `MembershipState`_ optional            
======================== ====== ================== ======== ===========

RemoveMembershipResult
----------------------

============================= ====== ================ ======== =============================================================================
Field                         Number Type             Label    Description                                                                  
============================= ====== ================ ======== =============================================================================
:code:`request_header`        100    `RequestHeader`_ optional                                                                              
:code:`member_id`             1      `MemberId`_      optional                                                                              
:code:`attached_roster_names` 4      string           repeated optional FailureReason reason = 2; repeated RosterId attached_roster_ids = 3;
============================= ====== ================ ======== =============================================================================

RemoveMembershipsResponse
-------------------------

========================================= ====== ========================= ======== ===================================================
Field                                     Number Type                      Label    Description                                        
========================================= ====== ========================= ======== ===================================================
:code:`results`                           1      `RemoveMembershipResult`_ repeated                                                    
:code:`retention_settings`                3      `RetentionSettings`_      optional repeated RosterId roster_ids_without_view_acls = 2;
:code:`retention_settings_group_revision` 4      `WriteRevision`_          optional                                                    
========================================= ====== ========================= ======== ===================================================

HideGroupRequest
----------------

====================== ====== ================ ======== ===========
Field                  Number Type             Label    Description
====================== ====== ================ ======== ===========
:code:`request_header` 100    `RequestHeader`_ optional            
:code:`id`             1      `GroupId`_       optional            
:code:`hide`           2      bool             optional            
====================== ====== ================ ======== ===========

HideGroupResponse
-----------------

===================== ====== ================= ======== ===========
Field                 Number Type              Label    Description
===================== ====== ================= ======== ===========
:code:`read_state`    1      `GroupReadState`_ optional            
:code:`user_revision` 3      `WriteRevision`_  optional            
===================== ====== ================= ======== ===========

InviteNotificationSettings
--------------------------

============== ====== ================================================ ======== ===========
Field          Number Type                                             Label    Description
============== ====== ================================================ ======== ===========
:code:`option` 1      `InviteNotificationSettings.NotificationOption`_ repeated            
============== ====== ================================================ ======== ===========

InviteNotificationSettings.NotificationOption
---------------------------------------------

========================== ====== ===========
Name                       Number Description
========================== ====== ===========
:code:`UNSPECIFIED`        0                 
:code:`EMAIL_NOTIFICATION` 1                 
========================== ====== ===========

CreateMembershipRequest
-----------------------

============================= ====== ============================= ======== ===========
Field                         Number Type                          Label    Description
============================= ====== ============================= ======== ===========
:code:`request_header`        100    `RequestHeader`_              optional            
:code:`member_ids`            2      `MemberId`_                   repeated            
:code:`invitee_member_infos`  8      `InviteeMemberInfo`_          repeated            
:code:`membership_state`      3      `MembershipState`_            optional            
:code:`group_id`              4      `GroupId`_                    optional            
:code:`notification_settings` 5      `InviteNotificationSettings`_ optional            
============================= ====== ============================= ======== ===========

CreateMembershipResponse
------------------------

========================================= ====== ========================= ======== ===========
Field                                     Number Type                      Label    Description
========================================= ====== ========================= ======== ===========
:code:`results`                           2      `CreateMembershipResult`_ repeated            
:code:`group_revision`                    4      `WriteRevision`_          repeated            
:code:`retention_settings`                5      `RetentionSettings`_      optional            
:code:`retention_settings_group_revision` 6      `WriteRevision`_          optional            
========================================= ====== ========================= ======== ===========

MarkGroupReadstateRequest
-------------------------

====================== ====== ================ ======== ===========
Field                  Number Type             Label    Description
====================== ====== ================ ======== ===========
:code:`request_header` 100    `RequestHeader`_ optional            
:code:`id`             1      `GroupId`_       optional            
:code:`last_read_time` 2      int64            optional            
====================== ====== ================ ======== ===========

MarkGroupReadstateResponse
--------------------------

===================== ====== ================= ======== ===========
Field                 Number Type              Label    Description
===================== ====== ================= ======== ===========
:code:`read_state`    1      `GroupReadState`_ optional            
:code:`user_revision` 2      `WriteRevision`_  optional            
===================== ====== ================= ======== ===========

SetPresenceSharedRequest
------------------------

======================= ====== ================ ======== ===========
Field                   Number Type             Label    Description
======================= ====== ================ ======== ===========
:code:`request_header`  100    `RequestHeader`_ optional            
:code:`presence_shared` 1      bool             optional            
======================= ====== ================ ======== ===========

SetPresenceSharedResponse
-------------------------

===================== ====== ================ ======== ===========
Field                 Number Type             Label    Description
===================== ====== ================ ======== ===========
:code:`user_status`   1      `UserStatus`_    optional            
:code:`user_revision` 2      `WriteRevision`_ optional            
===================== ====== ================ ======== ===========

SetDndDurationRequest
---------------------

================================= ====== ============================== ======== =================
Field                             Number Type                           Label    Description      
================================= ====== ============================== ======== =================
:code:`request_header`            100    `RequestHeader`_               optional                  
:code:`current_dnd_state`         2      `SetDndDurationRequest.State`_ optional                  
:code:`new_dnd_duration_usec`     1      int64                          optional oneof DndExpiry {
:code:`dnd_expiry_timestamp_usec` 3      int64                          optional }                
================================= ====== ============================== ======== =================

SetDndDurationRequest.State
---------------------------

================= ====== ===========
Name              Number Description
================= ====== ===========
:code:`UNKNOWN`   0                 
:code:`AVAILABLE` 1                 
:code:`DND`       2                 
================= ====== ===========

SetDndDurationResponse
----------------------

===================== ====== ================ ======== ===========
Field                 Number Type             Label    Description
===================== ====== ================ ======== ===========
:code:`user_status`   1      `UserStatus`_    optional            
:code:`user_revision` 2      `WriteRevision`_ optional            
===================== ====== ================ ======== ===========

UpdateGroupRequest
------------------

====================== ====== ================================ ======== ===================================================================
Field                  Number Type                             Label    Description                                                        
====================== ====== ================================ ======== ===================================================================
:code:`request_header` 100    `RequestHeader`_                 optional                                                                    
:code:`space_id`       1      `SpaceId`_                       optional                                                                    
:code:`update_masks`   4      `UpdateGroupRequest.UpdateMask`_ repeated                                                                    
:code:`name`           2      string                           optional                                                                    
:code:`visibility`     5      `GroupVisibility`_               optional optional AvatarInfo avatar_info = 6; optional ?? space_details = 7;
====================== ====== ================================ ======== ===================================================================

UpdateGroupRequest.UpdateMask
-----------------------------

=============================== ====== ===========
Name                            Number Description
=============================== ====== ===========
:code:`UPDATE_MASK_UNSPECIFIED` 0                 
:code:`NAME`                    1                 
:code:`VISIBILITY`              2                 
:code:`AVATAR_INFO`             3                 
:code:`SPACE_DETAILS`           4                 
:code:`SHARED_DRIVE`            5                 
=============================== ====== ===========

UpdateGroupResponse
-------------------

====================== ====== ================ ======== ===========
Field                  Number Type             Label    Description
====================== ====== ================ ======== ===========
:code:`group`          1      `Group`_         optional            
:code:`group_revision` 2      `WriteRevision`_ optional            
====================== ====== ================ ======== ===========

BlockEntityRequest
------------------

====================== ====== ================ ======== ==============
Field                  Number Type             Label    Description   
====================== ====== ================ ======== ==============
:code:`request_header` 100    `RequestHeader`_ optional               
:code:`user_id`        1      `UserId`_        optional oneof Entity {
:code:`group_id`       2      `GroupId`_       optional               
:code:`blocked`        3      bool             optional }             
:code:`reported`       4      bool             optional               
====================== ====== ================ ======== ==============

BlockEntityResponse
-------------------

===================== ====== ================= ======== ===========
Field                 Number Type              Label    Description
===================== ====== ================= ======== ===========
:code:`read_state`    1      `GroupReadState`_ optional            
:code:`user_revision` 2      `WriteRevision`_  optional            
===================== ====== ================= ======== ===========

SetCustomStatusRequest
----------------------

============================================= ====== ================ ======== ==========================
Field                                         Number Type             Label    Description               
============================================= ====== ================ ======== ==========================
:code:`request_header`                        100    `RequestHeader`_ optional                           
:code:`custom_status`                         1      `CustomStatus`_  optional                           
:code:`custom_status_expiry_timestamp_usec`   2      int64            optional oneof CustomStatusTiming {
:code:`custom_status_remaining_duration_usec` 3      int64            optional }                         
============================================= ====== ================ ======== ==========================

SetCustomStatusResponse
-----------------------

===================== ====== ================ ======== ===========
Field                 Number Type             Label    Description
===================== ====== ================ ======== ===========
:code:`user_status`   1      `UserStatus`_    optional            
:code:`user_revision` 2      `WriteRevision`_ optional            
===================== ====== ================ ======== ===========

WriteRevision
-------------

====================== ====== ===== ======== ===========
Field                  Number Type  Label    Description
====================== ====== ===== ======== ===========
:code:`timestamp`      1      int64 optional            
:code:`prev_timestamp` 2      int64 optional            
====================== ====== ===== ======== ===========

ReadRevision
------------

================= ====== ===== ======== ===========
Field             Number Type  Label    Description
================= ====== ===== ======== ===========
:code:`timestamp` 1      int64 optional            
================= ====== ===== ======== ===========

ReferenceRevision
-----------------

================= ====== ===== ======== ===========
Field             Number Type  Label    Description
================= ====== ===== ======== ===========
:code:`timestamp` 1      int64 optional            
================= ====== ===== ======== ===========

UserType
--------

============= ====== ===========
Name          Number Description
============= ====== ===========
:code:`HUMAN` 0                 
:code:`BOT`   1                 
============= ====== ===========

InviteCategory
--------------

====================== ====== ===========
Name                   Number Description
====================== ====== ===========
:code:`UNKNOWN_INVITE` 0                 
:code:`REGULAR_INVITE` 1                 
:code:`SPAM_INVITE`    2                 
====================== ====== ===========

Presence
--------

========================== ====== ===========
Name                       Number Description
========================== ====== ===========
:code:`UNDEFINED_PRESENCE` 0                 
:code:`ACTIVE`             1                 
:code:`INACTIVE`           2                 
:code:`UNKNOWN`            3                 
:code:`SHARING_DISABLED`   4                 
========================== ====== ===========

DndState_State
--------------

=============================== ====== ===========
Name                            Number Description
=============================== ====== ===========
:code:`DND_STATE_STATE_UNKNOWN` 0                 
:code:`AVAILABLE`               1                 
:code:`DND`                     2                 
=============================== ====== ===========

TypingState
-----------

===================== ====== ===========
Name                  Number Description
===================== ====== ===========
:code:`UNKNOWN_STATE` 0                 
:code:`TYPING`        1                 
:code:`STOPPED`       2                 
===================== ====== ===========

GroupUnsupportedReason
----------------------

============================================ ====== ===========
Name                                         Number Description
============================================ ====== ===========
:code:`GROUP_UNSUPPORTED_REASON_UNSPECIFIED` 0                 
:code:`GROUP_DISABLED_ON_CLIENT`             1                 
:code:`GROUP_DISABLED_ON_SERVER`             2                 
============================================ ====== ===========

GroupSupportLevel
-----------------

======================= ====== ===========
Name                    Number Description
======================= ====== ===========
:code:`UNSUPPORTED`     0                 
:code:`DATA_SUPPORTED`  1                 
:code:`FULLY_SUPPORTED` 2                 
======================= ====== ===========

NotificationCause
-----------------

================================== ====== ===========
Name                               Number Description
================================== ====== ===========
:code:`UNKNOWN_NOTIFICATION_CAUSE` 0                 
:code:`DIRECT_MESSAGE`             1                 
:code:`AT_MENTION`                 2                 
:code:`NEW_TOPIC`                  3                 
:code:`SUBSCRIBED_TOPIC_REPLY`     4                 
:code:`UNSUBSCRIBED_TOPIC_REPLY`   5                 
================================== ====== ===========

EventOrigin
-----------

========================== ====== ===========
Name                       Number Description
========================== ====== ===========
:code:`ORIGIN_UNSPECIFIED` 0                 
:code:`ANDROID`            100               
:code:`ANDROID_DEV`        101               
:code:`ANDROID_PRODTEST`   102               
:code:`ANDROID_STAGING`    103               
:code:`ANDROID_PROD`       104               
:code:`IOS`                110               
:code:`IOS_DEV`            111               
:code:`IOS_PRODTEST`       112               
:code:`IOS_STAGING`        113               
:code:`IOS_PROD`           114               
:code:`WEB_ORIGIN`         120               
:code:`WEB_DEV`            121               
:code:`WEB_PRODTEST`       122               
:code:`WEB_PROD`           123               
:code:`WEB_STAGING`        124               
:code:`DESKTOP_ORIGIN`     125               
:code:`DESKTOP_DEV`        126               
:code:`DESKTOP_PRODTEST`   127               
:code:`DESKTOP_STAGING`    128               
:code:`DESKTOP_PROD`       129               
:code:`COMPANION_ORIGIN`   130               
:code:`COMPANION_DEV`      131               
:code:`COMPANION_PRODTEST` 132               
:code:`COMPANION_STAGING`  133               
:code:`COMPANION_PROD`     134               
:code:`WEB_GMAIL_ORIGIN`   135               
:code:`WEB_GMAIL_DEV`      136               
:code:`WEB_GMAIL_PRODTEST` 137               
:code:`WEB_GMAIL_STAGING`  138               
:code:`WEB_GMAIL_PROD`     139               
:code:`WEB_PWA_ORIGIN`     140               
:code:`WEB_PWA_DEV`        141               
:code:`WEB_PWA_PRODTEST`   142               
:code:`WEB_PWA_STAGING`    143               
:code:`WEB_PWA_PROD`       144               
========================== ====== ===========

Platform
--------

========================== ====== ===========
Name                       Number Description
========================== ====== ===========
:code:`UNDEFINED_PLATFORM` 0                 
:code:`WEB`                1                 
:code:`MOBILE`             2                 
:code:`DRONE`              3                 
:code:`INTEROP_PRESENCE`   4                 
:code:`CLASSIC_DESKTOP`    5                 
:code:`CLASSIC_PHONE`      6                 
:code:`CLASSIC_UNKNOWN`    7                 
:code:`WEB_GMAIL`          8                 
========================== ====== ===========

AnnotationType
--------------

======================================== ====== ===========
Name                                     Number Description
======================================== ====== ===========
:code:`ANNOTATION_TYPE_UNKNOWN`          0                 
:code:`URL`                              1                 
:code:`DRIVE_FILE`                       2                 
:code:`DRIVE_DOC`                        3                 
:code:`DRIVE_SHEET`                      4                 
:code:`DRIVE_SLIDE`                      5                 
:code:`USER_MENTION`                     6                 
:code:`VIDEO`                            7                 
:code:`FORMAT_DATA`                      8                 
:code:`IMAGE`                            9                 
:code:`PDF`                              10                
:code:`VIDEO_CALL`                       11                
:code:`MEMBERSHIP_CHANGED`               12                
:code:`UPLOAD_METADATA`                  13                
:code:`ROOM_UPDATED`                     14                
:code:`INVITATION`                       15                
:code:`SLASH_COMMAND`                    16                
:code:`GSUITE_INTEGRATION`               17                
:code:`DRIVE_FORM`                       18                
:code:`GROUP_RETENTION_SETTINGS_UPDATED` 19                
:code:`BABEL_PLACEHOLDER`                20                
:code:`READ_RECEIPTS_SETTINGS_UPDATED`   21                
:code:`INCOMING_WEBHOOK_CHANGED`         22                
:code:`INTEGRATION_CONFIG_UPDATED`       23                
:code:`CONSENTED_APP_UNFURL`             24                
======================================== ====== ===========

DialInNumberClass
-----------------

ComGoogleRtcMeetingsV1DialInNumberClass

================================ ====== ===========
Name                             Number Description
================================ ====== ===========
:code:`NUMBER_CLASS_UNSPECIFIED` 0                 
:code:`LOW_COST`                 1                 
:code:`HIGH_COST`                2                 
:code:`LEGACY`                   3                 
================================ ====== ===========

RecordingApplicationType
------------------------

ComGoogleRtcMeetingsV1RecordingApplicationType

============================================== ====== ===========
Name                                           Number Description
============================================== ====== ===========
:code:`RECORDING_APPLICATION_TYPE_UNSPECIFIED` 0                 
:code:`RECORDING`                              1                 
:code:`GLIVE_STREAM`                           3                 
:code:`BROADCAST`                              4                 
============================================== ====== ===========

BroadcastAccessPolicy
---------------------

ComGoogleRtcMeetingsV1BroadcastAccessPolicy

============================================== ====== ===========
Name                                           Number Description
============================================== ====== ===========
:code:`BROADCASTING_ACCESS_POLICY_UNSPECIFIED` 0                 
:code:`ORGANIZATION`                           1                 
:code:`PUBLIC`                                 2                 
============================================== ====== ===========

CallStatus
----------

=========================== ====== ===========
Name                        Number Description
=========================== ====== ===========
:code:`UNKNOWN_CALL_STATUS` 0                 
:code:`ACTIVE_CALL`         1                 
:code:`ENDED_CALL`          2                 
=========================== ====== ===========

RingStatus
----------

=========================== ====== ===========
Name                        Number Description
=========================== ====== ===========
:code:`UNKNOWN_RING_STATUS` 0                 
:code:`RINGING`             1                 
:code:`JOINED`              2                 
:code:`MISSED`              3                 
=========================== ====== ===========

UserAccountState
----------------

================================== ====== ===========
Name                               Number Description
================================== ====== ===========
:code:`USER_ACCOUNT_STATE_UNKNOWN` 0                 
:code:`ENABLED`                    1                 
:code:`DISABLED`                   2                 
:code:`DELETED`                    3                 
:code:`TEMPORARY_UNAVAILABLE`      4                 
================================== ====== ===========

MembershipRole
--------------

==================== ====== ===========
Name                 Number Description
==================== ====== ===========
:code:`ROLE_UNKNOWN` 0                 
:code:`ROLE_NONE`    1                 
:code:`ROLE_INVITEE` 2                 
:code:`ROLE_MEMBER`  3                 
:code:`ROLE_OWNER`   4                 
==================== ====== ===========

AppType
-------

============================ ====== ===========
Name                         Number Description
============================ ====== ===========
:code:`APP_TYPE_UNSPECIFIED` 0                 
:code:`APP`                  1                 
:code:`GSUITE_APP`           2                 
:code:`INCOMING_WEBHOOK`     3                 
============================ ====== ===========

MembershipState
---------------

=========================== ====== ===========
Name                        Number Description
=========================== ====== ===========
:code:`MEMBER_UNKNOWN`      0                 
:code:`MEMBER_INVITED`      1                 
:code:`MEMBER_JOINED`       2                 
:code:`MEMBER_NOT_A_MEMBER` 3                 
:code:`MEMBER_FAILED`       4                 
=========================== ====== ===========

SharedAttributeCheckerGroupType
-------------------------------

================================================ ====== ===========
Name                                             Number Description
================================================ ====== ===========
:code:`ATTRIBUTE_CHECKER_GROUP_TYPE_UNSPECIFIED` 0                 
:code:`ONE_TO_ONE_HUMAN_DM`                      1                 
:code:`ONE_TO_ONE_BOT_DM`                        2                 
:code:`IMMUTABLE_MEMBERSHIP_GROUP_DM`            3                 
:code:`FLAT_ROOM`                                4                 
:code:`THREADED_ROOM`                            5                 
:code:`IMMUTABLE_MEMBERSHIP_HUMAN_DM`            6                 
:code:`POST_ROOM`                                7                 
================================================ ====== ===========

